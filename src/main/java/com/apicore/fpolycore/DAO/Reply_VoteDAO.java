package com.apicore.fpolycore.DAO;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.apicore.fpolycore.Class.Reply;
import com.apicore.fpolycore.Class.Reply_Vote;
import org.springframework.data.repository.query.Param;

public interface Reply_VoteDAO extends JpaRepository<Reply_Vote, Integer> {
    @Query("select r from Reply_Vote r where r.reply=?1")
    List<Reply_Vote> find(Reply reply);

//    @Query("select COUNT(p) from Reply_Vote p where p.reply.id = :id and p.like=true")
//    Integer CountLike(@Param("id") Integer id);
//
//    @Query("select COUNT(p) from Reply_Vote p where p.reply.id = :id and p.dislike=true")
//    Integer CountDis(@Param("id") Integer id);
    @Query("select COUNT(p) from Reply_Vote p where p.reply.id = :id and p.likes=1")
    Integer CountLike(@Param("id") Integer id);

    @Query("select COUNT(p) from Reply_Vote p where p.reply.id = :id and p.dislikes=1")
    Integer CountDis(@Param("id") Integer id);
}
