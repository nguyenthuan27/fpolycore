package com.apicore.fpolycore.DAO;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.apicore.fpolycore.Class.post_vote;
import com.apicore.fpolycore.Class.Posts;
import com.apicore.fpolycore.Class.Reports;
import com.apicore.fpolycore.Class.Users;

public interface UsersDAO extends JpaRepository<Users, Integer> {

    @Query("select u from Users u  where u.email = :id")
    Users findemail(@Param("id") String email);

    @Query("select u from Users u  where u.email = :id")
    Optional<Users> findemail1(@Param("id") String email);

    @Query("select u from Users u join user_report ur on u.id = ur.users where u.id =?1")
    Reports findReportUserById(Integer id);

    @Query("select u from Users u join Reports r on u.id = r.users where u.id =?1")
    Reports findReportByUserId(Integer id);

    @Query("select u from Users u join Posts p on u.id = p.users where u.id= :id")
    public Users find(@Param("id") Integer id);

    @Query("select u from Users u join PointHistory ph on u.id = ph.users where u.id= :id")
    Users findPoint(@Param("id") Integer id);

    @Query("select u, p.dislike as dl, p.likes as l from Users u join post_vote p on u.id = p.users where p.posts = :id")
    public List<Users> findvote(@Param("id") Posts id);
}
