package com.apicore.fpolycore.DAO;

import org.springframework.data.jpa.repository.JpaRepository;

import com.apicore.fpolycore.Class.SystemNotification;


public interface SystemNotificationDAO extends JpaRepository<SystemNotification, Integer> {

}
