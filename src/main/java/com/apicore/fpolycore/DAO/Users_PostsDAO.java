package com.apicore.fpolycore.DAO;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.apicore.fpolycore.Class.Users_Posts;

public interface Users_PostsDAO extends JpaRepository<Users_Posts, Integer> {
    @Query("select p from Users_Posts p where p.posts_id = ?1 ORDER BY p.id desc")
    Users_Posts findAllbyId1(Integer id, PageRequest pageable);

    @Query(nativeQuery = true, value = "select * from users_posts where posts_id=?1 ORDER BY id desc")
    Users_Posts findstatus(Integer id);
    
    @Query("select count(p) from Users_Posts p where p.status = 0 and month(p.createAt) = month(now()) and  weekday(p.createAt) = :day")
    Integer findCanDuyet(@Param("day") Integer day);
    
    @Query("select count(p) from Users_Posts p where p.status = 0 and  month(:date) = month(p.createAt) and day(:date) - day(p.createAt) <=1")
    Integer findCanDuyetTheoNgay(@Param("date") Date date);
    
    @Query("select date(p.createAt) as ngay,count(p) from Users_Posts p where p.status = 0 and year(p.createAt) = :date and monthname(p.createAt) = :month group by (date(p.createAt)) order by (date(p.createAt)) asc")
    List<String> CountInMonth(@Param("date") Integer date,@Param("month") String month);
    
    @Query("select dayname(p.createAt) as thu,count(p) from Users_Posts p where p.status = 0 and month(:date) = month(p.createAt) and day(:date) - day(p.createAt) <=7group by (dayname(p.createAt))")
    List<String> CountInWeek(@Param("date") Date date);
    
    @Query("select date(p.createAt) as ngay,count(p) from Users_Posts p where p.status = 0 and ( date(p.createAt) between :from and :to) group by (date(p.createAt)) order by (date(p.createAt)) asc")
    List<String> CountBetween(@Param("from") Date from, @Param("to") Date to);
}
