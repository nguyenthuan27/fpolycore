package com.apicore.fpolycore.DAO;

import com.apicore.fpolycore.Class.Branches;
import com.apicore.fpolycore.Class.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface BranchesDAO extends JpaRepository<Branches, Integer> {
    @Query("select u from Branches u  where u.id = :id")
    Branches findemail(@Param("id") int email);
}
