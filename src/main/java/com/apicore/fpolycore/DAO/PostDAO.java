package com.apicore.fpolycore.DAO;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.apicore.fpolycore.Class.Posts;
import com.apicore.fpolycore.Class.Question;
import com.apicore.fpolycore.DTO.ThongKePost;

public interface PostDAO extends JpaRepository<Posts, Integer> {
    @Query("select p from Posts p join Question q on p.id = q.posts.id where p.id =?1")
    Question findAllbyId1(Integer id);

    @Query("select p from Posts p where p.id = :id")
    List<Posts> find(@Param("id") Integer id);

    @Query("select p from Posts p where p.id = :id")
    Posts findOne(@Param("id") Integer id);

    @Query("select p from Posts p where p.id = :id")
    Posts findLike(@Param("id") Integer id);

    @Query("select p from Posts p where p.Question = :id")
    Posts findQ(@Param("id") Integer id);

    @Query("select p from Posts p join Question q on p.id = q.posts.id where q.id = :id")
    Posts findP(@Param("id") Integer id);

    @Query("select p from Posts p where p.users.id = :id")
    List<Posts> findByUserId(@Param("id") Integer id);

    @Query("select p from Posts p join Question q on p.id = q.posts.id where p.display_status=false and p.anonymus= false ")
    List<Posts> findAllAll();

    @Query("select dayname(p.createAt) as thu, count(p) as soLuong from Posts p where month(:date) = month(p.createAt) and day(:date) - day(p.createAt) <=7 group by (dayname(p.createAt))")
    List<String> Count(@Param("date") Date date);
    
    @Query("select count(p) from Posts p where year(:date) = year(p.createAt) and monthname(p.createAt) = :month")
    Integer CountInMonth(@Param("date") Date date,@Param("month") String month);
    
    @Query("select count(p) from Posts p where date(p.createAt) between :dateFrom and :dateTo")
    Integer CountBetween(@Param("dateFrom") Date dateFrom, @Param("dateTo") Date dateTo);
    
    @Query("select month(p.createAt) as Thang, count(p) as soLuong from Posts p where  (year(p.createAt) = :year ) and QUARTER(p.createAt) = :num  group by extract(year_month from p.createAt)")
    List<String> CountQuater(@Param("year") Integer year,@Param("num") Integer num);

    @Query("select month(p.createAt) as thang, count(p) as soluong from Posts p where (year(p.createAt) = :createAt ) group by extract(year_month from p.createAt)")
    List<String> getThongKePost(Integer createAt);
    
    @Query("select p from Posts p  where p.type = true")
    List<Posts> findAS();
    @Query("select p from Posts p  where p.type = false ")
    List<Posts> findAS1();

    @Query("select p from Posts p  where p.type = false ")
    List<Posts> findBL();
}
