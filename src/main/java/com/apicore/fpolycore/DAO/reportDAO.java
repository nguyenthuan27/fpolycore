package com.apicore.fpolycore.DAO;

import java.util.Date;
import java.util.List;

import com.apicore.fpolycore.Class.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.apicore.fpolycore.Class.Reports;

public interface reportDAO extends JpaRepository<Reports, Integer> {
    @Query("select r from Reports r  where r.id =?1")
    Reports findReportQuestionById(Integer id);

//	@Query("select r from Reports r join ReportReply rl on r.id = rl.report where rl.id = :id")	
////	@Query("select r from Reports r where r.report_reply = :id")	
//	Reports findReportReplyById(Integer id);

    @Query("select rl.report from ReportReply rl where rl.id = :id")
    Reports findReport(@Param("id") Integer id);

//	@Query("select rl.report from ReportQuestion rq where rq.id = :id")
//	Reports findReportQ(@Param("id") Integer id);

    @Query("select r from Reports r where r.user_report =:id")
    Reports findReportByUserId(Integer id);

    @Query("select r from Reports r  where r.id =:id")
    Reports find(Integer id);
    
    @Query("select r from Reports r  where r.content =:str")
    List<Reports> findByContent(@Param("str")String str);
    
    @Query("select date(r.createAt) as ngay, count(r) as soluong from Reports r  where r.content =:str and date(r.createAt) between :dateFrom and :dateTo group by (date(r.createAt))")
    List<String> findByContentBetween(@Param("str")String str, @Param("dateFrom") Date dateFrom, @Param("dateTo") Date dateTo);

    @Query("select q.createAt as ngay,count(q) from Reports q where q.createAt between :start and :end group by ngay  ")
    List<String> findtk(@Param("start") Date start, @Param("end") Date end);

}
