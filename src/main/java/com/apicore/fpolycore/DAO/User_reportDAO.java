package com.apicore.fpolycore.DAO;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.apicore.fpolycore.Class.user_report;


public interface User_reportDAO extends JpaRepository<user_report, Integer> {
    @Query("select ur from user_report ur where ur.report.id=:id")
    user_report find(Integer id);

    @Query("select ur from user_report ur where ur.report.id=?1")
    List<user_report> findU(Integer id);

    @Query("select ur from user_report ur where ur.report.id=?1")
    user_report findQ(Integer id);
}
