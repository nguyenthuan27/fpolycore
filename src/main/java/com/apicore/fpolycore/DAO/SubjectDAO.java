package com.apicore.fpolycore.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.apicore.fpolycore.Class.Question;
import com.apicore.fpolycore.Class.Subject;

import antlr.collections.List;


public interface SubjectDAO extends JpaRepository<Subject, Integer> {
    @Query("select s from Subject s where s.id = :id")
    Subject find(@Param("id") Integer id);

    @Query(nativeQuery = true, value = "select * from datnfpoly.subject a where a.id in (select subject_id from datnfpoly.tags where posts_id = :id )")
    java.util.List<Subject> findNew(@Param("id") Integer id);
}
