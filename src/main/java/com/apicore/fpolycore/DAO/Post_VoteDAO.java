package com.apicore.fpolycore.DAO;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.apicore.fpolycore.Class.post_vote;
import com.apicore.fpolycore.Class.Posts;
import com.apicore.fpolycore.Class.Users;

public interface Post_VoteDAO extends JpaRepository<post_vote, Integer> {
    @Query("select p from post_vote p where p.posts.id = :id")
    List<post_vote> find(@Param("id") Integer id);

    @Query("select p from post_vote p where p.posts.id = :id and p.users.id= :user")
    post_vote findUser(@Param("id") Integer id, @Param("user") Integer user);

    @Query("select p from post_vote p where p.posts.id = :id")
    post_vote findOne(@Param("id") Integer id);

    @Query("select COUNT(p) from post_vote p where p.posts.id = :id and p.likes=true")
    Integer CountLike(@Param("id") Integer id);

    @Query("select COUNT(p) from post_vote p where p.posts.id = :id and p.dislike=true")
    Integer CountDis(@Param("id") Integer id);

//	@Query("select pv from Post_Vote pv join Posts p on pv.posts = p.id where p.id = :id")
//	public Post_Vote find(@Param("id") Integer id);

}
