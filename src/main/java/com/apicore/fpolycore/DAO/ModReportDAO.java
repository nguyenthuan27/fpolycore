package com.apicore.fpolycore.DAO;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.apicore.fpolycore.Class.user_report;

public interface ModReportDAO extends JpaRepository<user_report, Integer> {
    @Query("select ur from user_report ur where ur.report.id = :id")
    List<user_report> find(Integer id);

    @Query("select ur from user_report ur where ur.report.id = :id")
    user_report findOne(Integer id);
}
