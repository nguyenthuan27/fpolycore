package com.apicore.fpolycore.DAO;

import java.util.Date;
import java.util.List;

import com.apicore.fpolycore.DTO.StatisticalCount;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.apicore.fpolycore.Class.Question;
import com.apicore.fpolycore.Class.Reply;


public interface ReplyDAO extends JpaRepository<Reply, Integer> {
    @Query("select r from Reply r where r.question.id = :id and r.type = :type and r.status= true")
    List<Reply> find(@Param("id") Integer id, @Param("type") Boolean type);

    @Query("select r from Reply r where r.question.id = :id and r.type = :type and r.status= true ")
    List<Reply> find2(@Param("id") Integer id, @Param("type") Boolean type);

    @Query("select r from Reply r join ReportReply rp on r.id = rp.reply.id where rp.id = :id")
    Reply findR(@Param("id") Integer id);

    @Query("select r from Reply r where r.report_reply = :id")
    Reply findQ(@Param("id") Integer id);
    @Query("select count(q) from Reply q where q.question.id = :id and q.type=true ")
    Integer Count(@Param("id") Integer id);

//	@Query("select count(q) from Reply q where q.question.id = :id and q.type=false ")
//	Integer Count1(@Param("id") Integer id);

    @Query("select new StatisticalCount(d.users,COUNT(d)) from Reply d GROUP BY d.users ORDER BY COUNT(d) desc ")
    List<StatisticalCount> findS(PageRequest pageable);

    @Query("select new StatisticalCount(d.users,COUNT(d)) from Reply d where d.createAt BETWEEN ?1 and ?2 GROUP BY d.users ORDER BY COUNT(d) desc ")
    List<StatisticalCount> findS1D(Date date, Date date1);
   // @Query("select new StatisticalCount(d.users,COUNT(d)) from Reply d where  d.id in (select u.id from Reply u where u.createAt BETWEEN ?1 and ?2 )  GROUP BY d.users ORDER BY COUNT(d) desc ")


    @Query("select r from Reply r where r.users.id = :id ")
    List<Reply> findByUserId(@Param("id") Integer id);



//    @Query("select user_id from * (select r.users.id, count(r.users.id) as solan from Reply r group by r.users.id having count(r.users.id) < all(select count(r.users.id) from Reply r )) as bang1 where solan =(select max(solan) from (select r.users.id, count(r.users.id) as solan from Reply r group by r.users.id having count(r.users.id) < all(select count(r.users.id) from Reply r )) as bang2 )  ")
//    Integer CountTopUser();


//	@Query(nativeQuery = true,value ="SELECT user_id, COUNT(*) AS total FROM reply where user_id in (select id from users)  GROUP BY user_id ORDER BY total desc LIMIT 10")
//	List<StatisticalCount> Counttk();


//	@Query(nativeQuery = true,value ="select new StatisticalCount( COUNT(d),d.user_id ) from reply d GROUP BY d.user_id ORDER BY COUNT(d) desc LIMIT 10")
//	List<Object[]> findS();
    //select COUNT(*)  AS total , user_id from reply  GROUP BY user_id ORDER BY total desc LIMIT 10
    //select COUNT(*) AS total , user_id from datnfpoly.reply q GROUP BY user_id ORDER BY total desc LIMIT 10
    @Query("select r from Reply r where r.id = :id")
    Reply findRs(@Param("id") Integer id);
    @Query("select q.createAt as ngay,count(q) from Reply q where q.createAt between :start and :end group by ngay  ")
    List<String> findtk1(@Param("start") Date start, @Param("end") Date end);
}
