package com.apicore.fpolycore.DAO;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.apicore.fpolycore.Class.Question;
import com.apicore.fpolycore.Class.ReportQuestion;


public interface ReportQuestionDAO extends JpaRepository<ReportQuestion, Integer> {
    @Query("select rq from ReportQuestion rq where rq.id = :id")
    List<ReportQuestion> find(@Param("id") Integer id);

    @Query("select rq from ReportQuestion rq where rq.report.id = :id")
    ReportQuestion findOne(@Param("id") Integer id);

    @Query("select rq.question from ReportQuestion rq where rq.report.id = :id")
    Question findReport(@Param("id") Integer id);

    @Query("select rq from ReportQuestion rq where rq.report = :id")
    List<ReportQuestion> findReportL(@Param("id") Integer id);
}
