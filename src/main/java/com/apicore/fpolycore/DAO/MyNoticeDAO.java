package com.apicore.fpolycore.DAO;

import org.springframework.data.jpa.repository.JpaRepository;

import com.apicore.fpolycore.Class.notification;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface MyNoticeDAO extends JpaRepository<notification, Integer> {
    @Query("select ur from notification ur where ur.user_id = :id")
    List<notification> findOne(Integer id);
}
