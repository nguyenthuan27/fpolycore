package com.apicore.fpolycore.DAO;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.apicore.fpolycore.Class.Posts;
import com.apicore.fpolycore.Class.Question;
import com.apicore.fpolycore.Class.Reply;
import com.apicore.fpolycore.Class.Reports;


public interface QuestionDAO extends JpaRepository<Question, Integer> {
    @Query("select q from Question q join Reply r on q.id = r.question where q.id =?1")
    Question findAllbyId1(Integer id);

    @Query("select q from Question q where q.id = :id order by q.id desc ")
    List<Question> find(@Param("id") Integer id);

    @Query("select q from Question q where q.report_question = :id ")
    List<Question> findR(@Param("id") Integer id);

    @Query(nativeQuery = true, value = "select  * from datnfpoly.question q where q.posts_id = :id order by q.id desc LIMIT 1 ")
    Question findQ(@Param("id") Integer id);

    @Query(nativeQuery = true, value = "select * from datnfpoly.question q where q.posts_id = :id order by q.id asc LIMIT 1 ")
    Question findNew(@Param("id") Integer id);


    //datnfpoly.question
    @Query("select q from Question q where q.id = :id order by q.id desc ")
    Question findQ1(@Param("id") Integer id);

    @Query("select q from Question q where q.posts.id = :id order by q.id desc ")
    Question findOne(@Param("id") Integer id);

    @Query("select count(q) from Question q where q.posts.id = :id ")
    Integer Count(@Param("id") Integer id);

    @Query("select q from Question q where q.report_question =:id")
    Question findP(Integer id);

    @Query("select q from Question q join ReportQuestion rp on q.id = rp.question.id where rp.id = :id")
    Question findReportReply(@Param("id") Integer id);

    @Query("select rq.question from ReportQuestion rq where rq.id = :id")
    Question findQuestion(@Param("id") Integer id);

    @Query("select dayname(p.createAt) as thu, count(p) as soLuong from Question p where month(:date) = month(p.createAt) and day(:date) - day(p.createAt) <=7 group by (dayname(p.createAt))")
    List<String> Count(@Param("date") Date date);
    
    @Query("select count(q) from Question q where month(:date) = month(q.createAt) and day(:date) - day(q.createAt) <=1")
    Integer CountToday(@Param("date") Date date);
    
    @Query("select date(q.createAt) as ngay, count(q) as soLuong from Question q where (year(q.createAt) = :date ) and monthname(q.createAt) = :month group by (date(q.createAt)) order by (date(q.createAt)) asc ")
    List<String> CountInMonth(@Param("date") Integer date,@Param("month") String month);
    
    @Query("select date(q.createAt) as ngay, count(q) as soLuong from Question q where date(q.createAt) between :dateFrom and :dateTo group by (date(q.createAt))")
    List<String> CountBetween(@Param("dateFrom") Date dateFrom, @Param("dateTo") Date dateTo);
    
    @Query("select QUARTER(q.createAt) as Thang, count(q) as soLuong from Question q where QUARTER(q.createAt) = :num and (year(q.createAt) = :createAt ) group by extract(year_month from q.createAt)")
    List<String> CountQuater(@Param("createAt") Integer createAt,@Param("num") Integer num);
    
    
    @Query("select count(q) from Question q where not exists ( select r from com.apicore.fpolycore.Class.Reply r where r.question.id = q.id  ) and ((month(now()) = month(q.createAt)) and (day(now()) = day(q.createAt)))")
    Integer findNoAnswerQuestionToday(@Param("date") Date date);
    
    @Query("select date(q.createAt) as ngay,count(q) from Question q where not exists ( select r from com.apicore.fpolycore.Class.Reply r where r.question.id = q.id  ) and (date(q.createAt) between :dateFrom and :dateTo) group by (date(q.createAt)) order by (date(q.createAt)) asc")
    List<String> findNoAnswerQuestionBetween(@Param("dateFrom") Date dateFrom, @Param("dateTo") Date dateTo);
    
    @Query("select dayname(q.createAt) as thu,count(q) from Question q where not exists ( select r from com.apicore.fpolycore.Class.Reply r where r.question.id = q.id  ) and ((month(now()) = month(q.createAt)) and (day(now()) - day(q.createAt) <=7)) group by (dayname(q.createAt))")
    List<String> findNoAnswerQuestionInWeek(@Param("date") Date date);
    
    @Query("select date(q.createAt) as ngay,count(q) from Question q where not exists ( select r from com.apicore.fpolycore.Class.Reply r where r.question.id = q.id )and year(q.createAt) = :year and monthname(q.createAt) = :month group by (date(q.createAt))")
    List<String> findNoAnswerQuestionInMonth(@Param("month") String month, @Param("year") Integer year);

    @Query("select q.createAt as ngay,count(q) from Question q where q.createAt between :start and :end group by ngay  ")
    List<String> findtk(@Param("start") Date start, @Param("end") Date end);
}
