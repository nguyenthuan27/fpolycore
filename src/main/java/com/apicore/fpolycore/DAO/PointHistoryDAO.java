package com.apicore.fpolycore.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.apicore.fpolycore.Class.PointHistory;


public interface PointHistoryDAO extends JpaRepository<PointHistory, Integer> {
//	 @Query("select ph from PointHistory ph where INSTR(ph.note,  ':str') >0 ")
	 @Query("select ph.type from PointHistory ph where ph.note = :str and ph.users.id = :uid")
	 String find(String str, Integer uid);
}
