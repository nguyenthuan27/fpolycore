package com.apicore.fpolycore.DAO;

import org.springframework.data.jpa.repository.JpaRepository;


import com.apicore.fpolycore.Class.PostApproval;

public interface PostApprovalDAO extends JpaRepository<PostApproval, Integer> {

}
