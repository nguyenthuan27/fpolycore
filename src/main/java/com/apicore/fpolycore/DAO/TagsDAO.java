package com.apicore.fpolycore.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.apicore.fpolycore.Class.Tags;


public interface TagsDAO extends JpaRepository<Tags, Integer> {
    @Query("select t from Tags t where t.id = :id")
    Tags findOne(@Param("id") Integer id);
}
