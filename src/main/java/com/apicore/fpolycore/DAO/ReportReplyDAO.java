package com.apicore.fpolycore.DAO;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.apicore.fpolycore.Class.Reply;
import com.apicore.fpolycore.Class.ReportReply;

public interface ReportReplyDAO extends JpaRepository<ReportReply, Integer> {
    @Query("select rl from ReportReply rl where rl.id = :id")
    List<ReportReply> find(@Param("id") Integer id);

    @Query("select rl from ReportReply rl where rl.reply = :id")
    ReportReply findOne(@Param("id") Integer id);

    @Query("select rl from ReportReply rl where rl.report.id = :id")
    ReportReply findReport(@Param("id") Integer id);

    @Query("select rl.reply from ReportReply rl where rl.report.id = :id")
    Reply findReply(@Param("id") Integer id);

}
