package com.apicore.fpolycore.basicApi;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.apicore.fpolycore.Class.PointHistory;
import com.apicore.fpolycore.Class.Subject;
import com.apicore.fpolycore.Class.Tags;
import com.apicore.fpolycore.Class.Users;
import com.apicore.fpolycore.Class.notification;
import com.apicore.fpolycore.DAO.MyNoticeDAO;
import com.apicore.fpolycore.DAO.PointHistoryDAO;
import com.apicore.fpolycore.DAO.UsersDAO;
import com.apicore.fpolycore.DTO.PointHistoryDTO;
import com.apicore.fpolycore.DTO.subjectDTO;
import com.apicore.fpolycore.DTO.tagsInfoDTO;


@CrossOrigin("*")
@RestController
@RequestMapping("pointHistory")
public class PointHistoryAPI {
    @Autowired
    PointHistoryDAO pointHistoryDAO;
    @Autowired
    UsersDAO userDAO;

    @GetMapping("/get")
    public ResponseEntity<List<PointHistory>> getfull() {
        return ResponseEntity.ok(pointHistoryDAO.findAll());
    }

    @PostMapping("/post")
    public ResponseEntity<PointHistory> post(@RequestBody PointHistory point, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.badRequest().build();
        } else {
            if (pointHistoryDAO.existsById(point.getId())) {
                return ResponseEntity.badRequest().build();
            }
            pointHistoryDAO.save(point);
            return ResponseEntity.ok(point);
        }

    }

    @PutMapping("/put/{x}")
    public ResponseEntity<PointHistory> put(@PathVariable("x") Integer id, @RequestBody PointHistory point, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.badRequest().build();
        } else {
            if (pointHistoryDAO.existsById(point.getId()) && id == point.getId()) {
                pointHistoryDAO.save(point);
                return ResponseEntity.ok(point);
            }
            return ResponseEntity.badRequest().build();
        }
    }

    @DeleteMapping("/delete/{x}")
    public ResponseEntity<Void> delete(@PathVariable("x") Integer id) {
        if (!pointHistoryDAO.existsById(id)) {
            return ResponseEntity.ok().build();
        }
        pointHistoryDAO.deleteById(id);
        return ResponseEntity.ok().build();
    }

    public List<PointHistoryDTO> getList() {
        List<PointHistory> listPointHis = pointHistoryDAO.findAll();
        List<PointHistoryDTO> list = new ArrayList<>();
        if (listPointHis != null) {
            for (PointHistory dl : listPointHis) {
                Users user = userDAO.findPoint(dl.getId());

                PointHistoryDTO phdto = new PointHistoryDTO(user, null, null, null, null);
                list.add(phdto);
            }
        }
        return list;

    }
}
