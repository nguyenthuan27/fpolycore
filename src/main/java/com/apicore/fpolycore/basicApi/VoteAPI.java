package com.apicore.fpolycore.basicApi;

import java.util.List;

import com.apicore.fpolycore.Class.*;
import com.apicore.fpolycore.DTO.PostGetAll;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.apicore.fpolycore.DAO.PostDAO;
import com.apicore.fpolycore.DAO.Post_VoteDAO;
import com.apicore.fpolycore.DAO.Reply_VoteDAO;
import com.apicore.fpolycore.DAO.UsersDAO;
import com.apicore.fpolycore.DTO.PostVoteDTO;
import com.apicore.fpolycore.DTO.VoteDTO;

@CrossOrigin("*")
@RestController
@RequestMapping("vote")
public class VoteAPI {
    @Autowired
    Post_VoteDAO Post_VoteDAO;
    @Autowired
    Reply_VoteDAO Reply_VoteDAO;
    @Autowired
    UsersDAO userDAO;
    @Autowired
    PostDAO postDAO;

    // Post_Vote
    @GetMapping("/getAll")
    public ResponseEntity<List<post_vote>> getPost() {
        return ResponseEntity.ok(Post_VoteDAO.findAll());
    }

    @PostMapping("/action")
    public ResponseEntity<String> postPost(@RequestBody VoteDTO postVote, BindingResult result) {
        if (result.hasErrors()) {
//				return ResponseEntity.badRequest().build();
            return ResponseEntity.ok("1");
        } else {
            if (!postDAO.existsById(postVote.getPostId())) {
                //return ResponseEntity.badRequest().build();
                return ResponseEntity.ok("2");
            }
            if (!userDAO.existsById(postVote.getUserId())) {
//				return ResponseEntity.badRequest().build();
                return ResponseEntity.ok("3");
            }
            Users user = userDAO.getById(postVote.getUserId());
            Posts post = postDAO.getById(postVote.getPostId());
            if (user != null && post != null) {
                post_vote postVote1 = Post_VoteDAO.findUser(postVote.getPostId(), postVote.getUserId());
                if (postVote1 != null) {
                    postVote1.setLikes(postVote.getLike());
                    postVote1.setDislike(postVote.getDislike());
                    Post_VoteDAO.save(postVote1);
                } else {
                    post_vote pv = new post_vote();
                    pv.setLikes(postVote.getLike());
                    pv.setPosts(post);
                    pv.setUsers(user);
                    pv.setDislike(postVote.getDislike());
                    try {
                        Post_VoteDAO.save(pv);
                    } catch (Exception e) {
                        return ResponseEntity.ok(e.toString());
                    }
                }
                return ResponseEntity.ok("thành công");
            } else {
                return ResponseEntity.badRequest().build();
            }
        }
    }

    @PutMapping("/update/{x}")
    public ResponseEntity<post_vote> putPost(@PathVariable("x") Integer id, @RequestBody post_vote postVote,
                                             BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.badRequest().build();
        } else {
            if (Post_VoteDAO.existsById(postVote.getId()) && id == postVote.getId()) {
                Post_VoteDAO.save(postVote);
                return ResponseEntity.ok(postVote);
            }
            return ResponseEntity.badRequest().build();
        }
    }

    @DeleteMapping("/delete/{x}")
    public ResponseEntity<Void> deletePost(@PathVariable("x") Integer id) {
        if (!Post_VoteDAO.existsById(id)) {
            return ResponseEntity.ok().build();
        }
        Post_VoteDAO.deleteById(id);
        return ResponseEntity.ok().build();
    }

    // Reply_Vote
    @GetMapping("/reply/get")
    public ResponseEntity<List<Reply_Vote>> getfull() {
        return ResponseEntity.ok(Reply_VoteDAO.findAll());
    }

    @PostMapping("/reply/post")
    public ResponseEntity<Reply_Vote> post(@RequestBody Reply_Vote replyVote, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.badRequest().build();
        } else {
            if (Reply_VoteDAO.existsById(replyVote.getId())) {
                return ResponseEntity.badRequest().build();
            }
            Reply_VoteDAO.save(replyVote);
            return ResponseEntity.ok(replyVote);
        }
    }

    @PutMapping("/reply/put/{x}")
    public ResponseEntity<Reply_Vote> put(@PathVariable("x") Integer id, @RequestBody Reply_Vote replyVote,
                                          BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.badRequest().build();
        } else {
            if (Reply_VoteDAO.existsById(replyVote.getId()) && id == replyVote.getId()) {
                Reply_VoteDAO.save(replyVote);
                return ResponseEntity.ok(replyVote);
            }
            return ResponseEntity.badRequest().build();
        }
    }

    @DeleteMapping("/reply/delete/{x}")
    public ResponseEntity<Void> delete(@PathVariable("x") Integer id) {
        if (!Reply_VoteDAO.existsById(id)) {
            return ResponseEntity.ok().build();
        }
        Reply_VoteDAO.deleteById(id);
        return ResponseEntity.ok().build();
    }
}
