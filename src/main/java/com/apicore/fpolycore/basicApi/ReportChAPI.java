package com.apicore.fpolycore.basicApi;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.apicore.fpolycore.Class.Question;
import com.apicore.fpolycore.Class.Reply;
import com.apicore.fpolycore.Class.ReportQuestion;
import com.apicore.fpolycore.Class.ReportReply;
import com.apicore.fpolycore.Class.Reports;
import com.apicore.fpolycore.Class.user_report;
import com.apicore.fpolycore.DAO.QuestionDAO;
import com.apicore.fpolycore.DAO.ReplyDAO;
import com.apicore.fpolycore.DAO.ReportQuestionDAO;
import com.apicore.fpolycore.DAO.ReportReplyDAO;
import com.apicore.fpolycore.DAO.User_reportDAO;
import com.apicore.fpolycore.DAO.reportDAO;
import com.apicore.fpolycore.DTO.ListUser_Report;
import com.apicore.fpolycore.DTO.questionVoteDTO;
import com.apicore.fpolycore.DTO.replyPostDTO;
import com.apicore.fpolycore.DTO.reportQuestionDTO;
import com.apicore.fpolycore.DTO.reportReplyDTO;

@CrossOrigin("*")
@RestController
@RequestMapping("reportCh")
public class ReportChAPI {
    @Autowired
    ReportQuestionDAO ReportQuestionDAO;
    @Autowired
    ReportReplyDAO ReportReplyDAO;
    @Autowired
    reportDAO ReportDAO;
    @Autowired
    QuestionDAO questionDAO;
    @Autowired
    ReplyDAO replyDAO;
    @Autowired
    User_reportDAO userReportDAO;

    // Report ALL
    @GetMapping("/get")
    public ResponseEntity<List<Reports>> getALL() {
        return ResponseEntity.ok(ReportDAO.findAll());
    }

    @PostMapping("/post")
    public ResponseEntity<Reports> postALL(@RequestBody Reports report, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.badRequest().build();
        } else {
            if (ReportDAO.existsById(report.getId())) {
                return ResponseEntity.badRequest().build();
            }
            ReportDAO.save(report);
            return ResponseEntity.ok(report);
        }

    }

    @PutMapping("/Put/{x}")
    public ResponseEntity<Reports> putALL(@PathVariable("x") Integer id, @RequestBody Reports report,
                                          BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.badRequest().build();
        } else {
            if (ReportDAO.existsById(report.getId()) && id == report.getId()) {
                ReportDAO.save(report);
                return ResponseEntity.ok(report);
            }
            return ResponseEntity.badRequest().build();
        }
    }

    @DeleteMapping("/Delete/{x}")
    public ResponseEntity<Void> deleteALL(@PathVariable("x") Integer id) {
        if (!ReportDAO.existsById(id)) {
            return ResponseEntity.ok().build();
        }
        ReportDAO.deleteById(id);
        return ResponseEntity.ok().build();
    }

    // Question
    @GetMapping("/getQuestion")
    public ResponseEntity<List<reportQuestionDTO>> getQuestion() {
        return ResponseEntity.ok(getListReportQuestion());
    }

    @GetMapping("/getQuestion/{x}")
    public ResponseEntity<reportQuestionDTO> getQuestionX(@PathVariable("x") Integer id) {
        return ResponseEntity.ok(getReportQuestion(id));
    }

    @PostMapping("/PostQuestion")
    public ResponseEntity<ReportQuestion> postQuestion(@RequestBody ReportQuestion reportQuestion,
                                                       BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.badRequest().build();
        } else {
            if (ReportQuestionDAO.existsById(reportQuestion.getId())) {
                return ResponseEntity.badRequest().build();
            }
            ReportQuestionDAO.save(reportQuestion);
            return ResponseEntity.ok(reportQuestion);
        }
    }

    @PutMapping("/PutQuestion/{x}")
    public ResponseEntity<ReportQuestion> putQuestion(@PathVariable("x") Integer id,
                                                      @RequestBody ReportQuestion reportQuestion, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.badRequest().build();
        } else {
            if (ReportQuestionDAO.existsById(reportQuestion.getId()) && id == reportQuestion.getId()) {
                ReportQuestionDAO.save(reportQuestion);
                return ResponseEntity.ok(reportQuestion);
            }
            return ResponseEntity.badRequest().build();
        }
    }

    @DeleteMapping("/DeleteQuestion/{x}")
    public ResponseEntity<Void> deleteQuestion(@PathVariable("x") Integer id) {
        if (!ReportQuestionDAO.existsById(id)) {
            return ResponseEntity.ok().build();
        }
        ReportQuestionDAO.deleteById(id);
        return ResponseEntity.ok().build();
    }

    // Reply
    @GetMapping("/getReply")
    public ResponseEntity<List<reportReplyDTO>> getReply() {
        return ResponseEntity.ok(getListReportReply());
    }

    @GetMapping("/getReply/{x}")
    public ResponseEntity<reportReplyDTO> getReplyX(@PathVariable("x") Integer id) {
        return ResponseEntity.ok(getReportReply(id));
    }

    @PostMapping("/PostReply")
    public ResponseEntity<ReportReply> postReply(@RequestBody ReportReply reportReply, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.badRequest().build();
        } else {
            if (ReportReplyDAO.existsById(reportReply.getId())) {
                return ResponseEntity.badRequest().build();
            }
            ReportReplyDAO.save(reportReply);
            return ResponseEntity.ok(reportReply);
        }
    }

    @PutMapping("/PutReply/{x}")
    public ResponseEntity<ReportReply> putReply(@PathVariable("x") Integer id, @RequestBody ReportReply reportReply,
                                                BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.badRequest().build();
        } else {
            if (ReportReplyDAO.existsById(reportReply.getId()) && id == reportReply.getId()) {
                ReportReplyDAO.save(reportReply);
                return ResponseEntity.ok(reportReply);
            }
            return ResponseEntity.badRequest().build();
        }
    }

    @DeleteMapping("/DeleteReply/{x}")
    public ResponseEntity<Void> deleteReply(@PathVariable("x") Integer id) {
        if (!ReportReplyDAO.existsById(id)) {
            return ResponseEntity.ok().build();
        }
        ReportReplyDAO.deleteById(id);
        return ResponseEntity.ok().build();
    }

    public List<reportQuestionDTO> getListReportQuestion() {
        List<ReportQuestion> listReportQuestion = ReportQuestionDAO.findAll();
        List<reportQuestionDTO> list = new ArrayList<>();
        List<user_report> listUserReport = userReportDAO.findAll();
        if (listReportQuestion != null) {
            for (ReportQuestion rp : listReportQuestion) {
                questionVoteDTO qdto = null;
                try {
                    Reports report = ReportDAO.findReportQuestionById(rp.getId());
                    Question q = questionDAO.findQ(rp.getId());
                    Question dateold = questionDAO.findNew(rp.getId());

                    if (q != null) {
                        qdto = new questionVoteDTO(q.getId(), q.getContent(), q.getPoint(), q.getImg(), q.getTitle(),
                                q.getCreateAt(), dateold.getCreateAt());
                    }

                    user_report urp = userReportDAO.find(rp.getId());
                    ListUser_Report urpdto = new ListUser_Report(urp.getId(), urp.getCreateAt(), urp.getStatus(), urp.getUsers());

                    reportQuestionDTO rpDTO = new reportQuestionDTO(rp.getId(), report, qdto, urpdto);
                    list.add(rpDTO);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        return list;
    }

    public reportQuestionDTO getReportQuestion(Integer id) {
        ReportQuestion ReportQuestion = ReportQuestionDAO.findOne(id);
        reportQuestionDTO rpDTO = new reportQuestionDTO();

        if (ReportQuestion != null) {

            questionVoteDTO qdto = null;
            Reports report = ReportDAO.findReportQuestionById(ReportQuestion.getId());
            Question q = questionDAO.findQ(ReportQuestion.getId());
            Question dateold = questionDAO.findNew(ReportQuestion.getId());

            if (q != null) {
                qdto = new questionVoteDTO(q.getId(), q.getContent(), q.getPoint(), q.getImg(), q.getTitle(),
                        q.getCreateAt(), dateold.getCreateAt());
            }

            user_report urp = userReportDAO.find(q.getId());
            ListUser_Report urpdto = new ListUser_Report(urp.getId(), urp.getCreateAt(), urp.getStatus(), urp.getUsers());

            rpDTO = new reportQuestionDTO(ReportQuestion.getId(), report, qdto, urpdto);

        }

        return rpDTO;
    }

    public List<reportReplyDTO> getListReportReply() {
        List<ReportReply> listReportReply = ReportReplyDAO.findAll();
        List<reportReplyDTO> list = new ArrayList<>();

        if (listReportReply != null) {
            for (ReportReply rp : listReportReply) {
                replyPostDTO rdto = null;
                try {
                    Reports report = ReportDAO.findReport(rp.getId());
//					ReportReply rr = ReportReplyDAO.findReport(rp.getId());
//					System.out.println(rr.getReport());
                    System.out.println(report.getId());
                    Reply r = replyDAO.findR(rp.getId());
                    System.out.println(r.getId());

                    if (r != null) {
                        rdto = new replyPostDTO(r.getId(), r.getUsers(), r.getContent(), r.getType() == true ? 1 : 0, r.getAnonymus() == true ? 1 : 0, r.getStatus() == true ? 1 : 0, r.getImg(), r.getCreateAt());
                    }
                    user_report urp = userReportDAO.find(report.getId());
                    if(urp!=null) {
                    	
                    
                    ListUser_Report urpdto = new ListUser_Report(urp.getId(), urp.getCreateAt(), urp.getStatus(), urp.getUsers());
                    reportReplyDTO rlDTO = new reportReplyDTO(rp.getId(), report, rdto, urpdto);
                    list.add(rlDTO);}
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        return list;
    }

    public reportReplyDTO getReportReply(Integer id) {
        ReportReply ReportReply = ReportReplyDAO.findOne(id);
        reportReplyDTO rrDTO = new reportReplyDTO();

        if (ReportReply != null) {
            replyPostDTO rdto = null;

            Reports report = ReportDAO.findReport(ReportReply.getId());
            Reply r = replyDAO.findR(ReportReply.getId());

            if (r != null) {
                rdto = new replyPostDTO(r.getId(), r.getUsers(), r.getContent(), r.getType() == true ? 1 : 0, r.getAnonymus() == true ? 1 : 0,
                        r.getStatus() == true ? 1 : 0, r.getImg(), r.getCreateAt());
            }
            user_report urp = userReportDAO.find(r.getId());
            ListUser_Report urpdto = new ListUser_Report(urp.getId(), urp.getCreateAt(), urp.getStatus(), urp.getUsers());
            rrDTO = new reportReplyDTO(ReportReply.getId(), report, rdto, urpdto);

        }

        return rrDTO;
    }
}
