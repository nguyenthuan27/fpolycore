package com.apicore.fpolycore.basicApi;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import com.apicore.fpolycore.Class.Users_Posts;
import com.apicore.fpolycore.DAO.Users_PostsDAO;

@CrossOrigin("*")
@RestController
@RequestMapping("users-posts")
public class Users_PostsAPI {
    @Autowired
    Users_PostsDAO Users_PostsDAO;

    @GetMapping("/get")
    public ResponseEntity<List<Users_Posts>> getfull() {
        return ResponseEntity.ok(Users_PostsDAO.findAll());
    }
    
    @GetMapping("/getChuaDuyet/{x}")
    public ResponseEntity<Integer> getChuaDuyet(@PathVariable("x") Integer x) {
        return ResponseEntity.ok(Users_PostsDAO.findCanDuyet(x));
    }
    
    @GetMapping("/getTrongTuan")
    public ResponseEntity<List<String>> getChuaDuyetTrongTuan() {
    	long millis=System.currentTimeMillis();  
    	java.util.Date date=new java.util.Date(millis);
        return ResponseEntity.ok(Users_PostsDAO.CountInWeek(date));
    }
    
    @GetMapping("/getChuaDuyetTrongNgay")
    public ResponseEntity<Integer> getChuaDuyetTrongNgay() {
    	long millis=System.currentTimeMillis();  
    	java.util.Date date=new java.util.Date(millis);
        return ResponseEntity.ok(Users_PostsDAO.findCanDuyetTheoNgay(date));
    }
    
    @GetMapping("/getTheoThang/{x}/{y}")
    public ResponseEntity<List<String>> getChuaDuyetTrongNgay(@PathVariable("x") String x, @PathVariable("y") Integer y) {
        return ResponseEntity.ok(Users_PostsDAO.CountInMonth(y,x));
    }
    
    @GetMapping("/getBetween/{x}/{y}")
    public ResponseEntity<List<String>> getBetween(@PathVariable("x") java.sql.Date from, @PathVariable("y") java.sql.Date to) {
        return ResponseEntity.ok(Users_PostsDAO.CountBetween(from, to));
    }

    @PostMapping("/post")
    public ResponseEntity<Users_Posts> post(@RequestBody Users_Posts postUser, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.badRequest().build();
        } else {
            if (Users_PostsDAO.existsById(postUser.getId())) {
                return ResponseEntity.badRequest().build();
            }
            Users_PostsDAO.save(postUser);
            return ResponseEntity.ok(postUser);
        }

    }

    @PutMapping("/put/{x}")
    public ResponseEntity<Users_Posts> put(@PathVariable("x") Integer id, @RequestBody Users_Posts postUser, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.badRequest().build();
        } else {
            if (Users_PostsDAO.existsById(postUser.getId()) && id == postUser.getId()) {
                Users_PostsDAO.save(postUser);
                return ResponseEntity.ok(postUser);
            }
            return ResponseEntity.badRequest().build();
        }
    }

    @DeleteMapping("/delete/{x}")
    public ResponseEntity<Void> delete(@PathVariable("x") Integer id) {
        if (!Users_PostsDAO.existsById(id)) {
            return ResponseEntity.ok().build();
        }
        Users_PostsDAO.deleteById(id);
        return ResponseEntity.ok().build();
    }
}
