package com.apicore.fpolycore.basicApi;

import java.sql.Timestamp;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.apicore.fpolycore.Class.Branches;
import com.apicore.fpolycore.Class.Users;
import com.apicore.fpolycore.DAO.BranchesDAO;


@CrossOrigin("*")
@RestController
@RequestMapping("branches")
public class BranchesAPI {
    @Autowired
    BranchesDAO BranchesDAO;

    @GetMapping("/get")
    public ResponseEntity<List<Branches>> getfull() {
        return ResponseEntity.ok(BranchesDAO.findAll());
    }
    
    @GetMapping("/get/{x}")
    public ResponseEntity<Optional<Branches>> get(@PathVariable("x") Integer id) {
//    	Date now = new Date();
//		Timestamp timestamp = new Timestamp(now.getTime());
//    	System.out.println(timestamp);
        return ResponseEntity.ok(BranchesDAO.findById(id));
    }

    @PostMapping("/post")
    public ResponseEntity<Branches> post(@Validated @RequestBody Branches branche, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.badRequest().build();
        } else {
            if (BranchesDAO.existsById(branche.getId())) {
                return ResponseEntity.badRequest().build();
            }
            BranchesDAO.save(branche);
            return ResponseEntity.ok(branche);
        }
    }

    @PutMapping("/put/{x}")
    public ResponseEntity<Branches> put(@PathVariable("x") Integer id, @RequestBody Branches branche, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.badRequest().build();
        } else {
            if (BranchesDAO.existsById(branche.getId()) && id == branche.getId()) {
                BranchesDAO.save(branche);
                return ResponseEntity.ok(branche);
            }
            return ResponseEntity.badRequest().build();
        }
    }

    @DeleteMapping("/delete/{x}")
    public ResponseEntity<Void> delete(@PathVariable("x") Integer id) {
        if (!BranchesDAO.existsById(id)) {
            return ResponseEntity.ok().build();
        }
        BranchesDAO.deleteById(id);
        return ResponseEntity.ok().build();
    }
}
