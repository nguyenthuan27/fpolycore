package com.apicore.fpolycore.basicApi;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

import com.apicore.fpolycore.Class.*;
import com.apicore.fpolycore.DAO.*;
import com.apicore.fpolycore.DTO.*;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin("*")
@RestController
@RequestMapping("question")
public class QuestionAPI {
    @Autowired
    QuestionDAO QuestionDAO;
    @Autowired
    PostDAO postDAO;
    @Autowired
    ReplyDAO replyDAO;
    @Autowired
    SubjectDAO subjectDAO;
    @Autowired
    Post_VoteDAO post_voteDAO;
    @Autowired
    Users_PostsDAO users_postsDAO;
    @Autowired
    TagsDAO tagsDAO;

    @GetMapping("/getAll")
    public ResponseEntity<List<questionDTO>> getfull() {
        return ResponseEntity.ok(getList());
    }

    @GetMapping("/getById/{x}")
    public ResponseEntity<questionDTO> get(@PathVariable("x") Integer id) {
        return ResponseEntity.ok(getById(id));
    }

    @GetMapping("/count")
    public ResponseEntity<List<String>> count() {
        long millis=System.currentTimeMillis();
        java.util.Date date=new java.util.Date(millis);
        return ResponseEntity.ok(QuestionDAO.Count(date));
    }

    @GetMapping("/countBetween/{x}/{y}")
    public ResponseEntity<List<String>> countBetween(@PathVariable("x") java.sql.Date from, @PathVariable("y") java.sql.Date to) {
        return ResponseEntity.ok(QuestionDAO.CountBetween(from, to));
    }

    @GetMapping("/countNoBetween/{x}/{y}")
    public ResponseEntity<List<String>> countBetweenN(@PathVariable("x") java.sql.Date from, @PathVariable("y") java.sql.Date to) {
        return ResponseEntity.ok(QuestionDAO.findNoAnswerQuestionBetween(from, to));
    }

    @GetMapping("/tk/{x}/{y}")
    public ResponseEntity< List<CountMonth>> tk( @PathVariable("x") java.sql.Date from, @PathVariable("y") java.sql.Date to) {
        Calendar c = Calendar.getInstance();
        c.setTime(to);
        c.add(Calendar.DATE,1);
        List<String> list= QuestionDAO.findtk(from,c.getTime());
        List<CountMonth> countMonths =new ArrayList<>();
        for (String n : list){
            CountMonth CountMonth =new CountMonth();
            String[] arr = n.split(",");
            String arr1= arr[0];
            Integer integer =arr[1]!=null?Integer.valueOf(arr[1]):0;
            CountMonth.setDay(arr1!=null?arr1:null);
            CountMonth.setCount(integer);
            countMonths.add(CountMonth);
        }
        return ResponseEntity.ok(countMonths);
    }

    @GetMapping("/countToday")
    public ResponseEntity<Integer> countToday() {
    	long millis=System.currentTimeMillis();
    	java.util.Date date=new java.util.Date(millis);
        return ResponseEntity.ok(QuestionDAO.CountToday(date));
    }

    @GetMapping("/countMonth/{x}/{y}")
    public ResponseEntity<List<CountMonth>> countInMonth(@PathVariable("x") String month, @PathVariable("y") Integer year) throws ParseException {
        List<String> list= QuestionDAO.CountInMonth(year, month);
        List<CountMonth> countMonths =new ArrayList<>();
        for (String n : list){
            CountMonth CountMonth =new CountMonth();
            String[] arr = n.split(",");
            String arr1= arr[0];
            Integer integer =arr[1]!=null?Integer.valueOf(arr[1]):0;
            CountMonth.setDay(arr1!=null?arr1:null);
            CountMonth.setCount(integer);
            countMonths.add(CountMonth);
        }
        return ResponseEntity.ok(countMonths);
    }

    @GetMapping("/countQuater/{x}/{y}")
    public ResponseEntity<List<CountMonth>> countQuater(@PathVariable("x") Integer num, @PathVariable("y") Integer year) {
        List<String> list= QuestionDAO.CountQuater(year,num);
        List<CountMonth> countMonths =new ArrayList<>();
        for (String n : list){
            CountMonth CountMonth =new CountMonth();
            String[] arr = n.split(",");
            String arr1= arr[0];
            Integer integer =arr[1]!=null?Integer.valueOf(arr[1]):0;
            CountMonth.setDay(arr1!=null?arr1:null);
            CountMonth.setCount(integer);
            countMonths.add(CountMonth);
        }
        return ResponseEntity.ok(countMonths);
    }
    
    @GetMapping("/getNoToday")
    public ResponseEntity<Integer> getNoAnswerQuesToday() {
    	long millis=System.currentTimeMillis();  
    	java.util.Date date=new java.util.Date(millis);
        return ResponseEntity.ok(QuestionDAO.findNoAnswerQuestionToday(date));
    }
    
    @GetMapping("/getNoInWeek")
    public ResponseEntity<List<String>> getNoAnswerQuesWeek() {
    	long millis=System.currentTimeMillis();  
    	java.util.Date date=new java.util.Date(millis);
        return ResponseEntity.ok(QuestionDAO.findNoAnswerQuestionInWeek(date));
    }
    
    @GetMapping("/getNoInMonth/{x}/{y}")
    public ResponseEntity<List<String>> getNoAnswerQuesMonth(@PathVariable("x") String month, @PathVariable("y") Integer year) {
        return ResponseEntity.ok(QuestionDAO.findNoAnswerQuestionInMonth(month, year));
    }
    
    @PostMapping("/post")
    public ResponseEntity<questionv2sDTO> post(@RequestBody Questionpost question, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.badRequest().build();
        } else {
            if(postDAO.existsById(question.getIdpost())==true){
                Posts posts = postDAO.getById(question.getIdpost());
                Date now = new Date();
                Timestamp timestamp = new Timestamp(now.getTime());
                //anh
                TimeUnit time = TimeUnit.MILLISECONDS;
                long timeInMilliSec =  time.toMinutes(posts.getCreateAt().getTime());
                long timeInMilliSec1 = time.toMinutes(timestamp.getTime());
                long datareturl = timeInMilliSec1-timeInMilliSec;
                System.out.println(datareturl);
                //anh
//               if(datareturl<10 && datareturl>=0 ){
                   Question question1  =new Question();
                   question1.setContent(question.getContent());
                   question1.setPoint(question.getPoint());
                   question1.setImg(question.getImg());
                   question1.setTitle(question.getTitle());
                   question1.setCreateAt(timestamp);
                   question1.setPosts(posts);
                   Question data= QuestionDAO.save(question1);
                   for(TagDTO tags:question.getTag()){
                       Tags tags1 =new Tags();
                       tags1.setPosts_id(data.getId());
                       tags1.setSubject_id(tags.getId());
                       tagsDAO.save(tags1);
                   }
                   questionv2sDTO questionv2sDTOs =new questionv2sDTO();
                   questionv2sDTOs.setIdqe(data.getId());
                   questionv2sDTOs.setIdposts(question.getIdpost());
                   questionv2sDTOs.setTilte(question.getTitle());
                   questionv2sDTOs.setContent(question.getContent());
                   questionv2sDTOs.setType(posts.getType()==true?1:0);
                   questionv2sDTOs.setAnonymus(posts.getAnonymus()==true?1:0);
                   questionv2sDTOs.setStatus(posts.getDisplay_status()==true?1:0);
                   questionv2sDTOs.setImg(question.getImg());
                   questionv2sDTOs.setCreateAt(data.getCreateAt());
                   questionv2sDTOs.setLike(0);
                   questionv2sDTOs.setDislike(0);
                   questionv2sDTOs.setPoint(question.getPoint());
                   questionv2sDTOs.setTag(question.getTag());
                   return ResponseEntity.ok(questionv2sDTOs);
//               }
//               else{
//                   return  ResponseEntity.status(500).varyBy("bạn không thể chỉnh sửa bài viết sau 10 phút khi mới tạo bài viết").build();
//               }
            }
            else{
                return ResponseEntity.badRequest().build();
            }
        }
    }

    @PostMapping("/postAll")
    public ResponseEntity<questionv2DTO> postss(@RequestBody PostGetAll postGetAll, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.badRequest().build();
        } else {
            return ResponseEntity.ok(getByIdsss(postGetAll.getId(), postGetAll.getUser()));
        }
    }

    @PutMapping("/put/{x}")
    public ResponseEntity<Question> put(@PathVariable("x") Integer id, @RequestBody Question question,
                                        BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.badRequest().build();
        } else {
            if (QuestionDAO.existsById(question.getId()) && id == question.getId()) {
                QuestionDAO.save(question);
                return ResponseEntity.ok(question);
            }
            return ResponseEntity.badRequest().build();
        }
    }

    @DeleteMapping("/delete/{x}")
    public ResponseEntity<Void> delete(@PathVariable("x") Integer id) {
        if (!QuestionDAO.existsById(id)) {
            return ResponseEntity.ok().build();
        }
        QuestionDAO.deleteById(id);
        return ResponseEntity.ok().build();
    }

    private List<questionDTO> getList() {
        List<questionDTO> list = new ArrayList<questionDTO>();
        List<Question> listqL = QuestionDAO.findAll();
        if (listqL != null) {
            for (Question listq : listqL) {
                List<replyQuestionDTO> listrepQuestiondto = new ArrayList<>();
                List<replyPostDTO> listrepPostdto = new ArrayList<>();
                List<Reply> listrep = replyDAO.find(listq.getId(), true);
                List<Reply> listrepP = replyDAO.find2(listq.getId(), false);
                for (Reply reply : listrep) {
                    replyQuestionDTO replydto = new replyQuestionDTO(reply.getId(), reply.getUsers(), reply.getContent(), reply.getType() == true ? 1 : 0, reply.getAnonymus() == true ? 1 : 0, reply.getStatus() == true ? 1 : 0, reply.getImg(), reply.getCreateAt());
                    listrepQuestiondto.add(replydto);
                }
                for (Reply reply : listrepP) {
                    replyPostDTO replydto = new replyPostDTO(reply.getId(), reply.getUsers(), reply.getContent(), reply.getType() == true ? 1 : 0, reply.getAnonymus() == true ? 1 : 0, reply.getStatus() == true ? 1 : 0, reply.getImg(), reply.getCreateAt());
                    listrepPostdto.add(replydto);
                }
                postqeDTO dto = getPostById(listq.getPosts().getId());
                if (dto != null) {
                    postqeDTONotUser dtoNotUser = new postqeDTONotUser(dto.getId(), dto.getType(), dto.getAnonymus(),
                            dto.getDisplay_status(), dto.getListVote());
                    list.add(new questionDTO(listq.getId(), listq.getContent(), listq.getPoint(), listq.getImg(),
                            listq.getTitle(), listq.getCreateAt(), dto.getUsersPost(), dtoNotUser, listrepQuestiondto,
                            listrepPostdto, dto.getTag()));
                }
            }
        }
        return list;
    }

    public questionDTO getById(Integer id) {
        Question listq = QuestionDAO.getById(id);
        questionDTO questionDTO = null;
        PageRequest page = PageRequest.of(0, 1);
        Users_Posts aBoolean = users_postsDAO.findAllbyId1(id, page);
        if (aBoolean == null ? aBoolean == null : aBoolean.getStatus() == true) {
            if (listq != null) {
                List<replyQuestionDTO> listrepQuestiondto = new ArrayList<>();
                List<replyPostDTO> listrepPostdtode = new ArrayList<>();
                List<Reply> listrep = replyDAO.find(listq.getId(), true);
                List<Reply> listrepP = replyDAO.find2(listq.getId(), false);
                for (Reply reply : listrep) {
                    replyQuestionDTO replydto = new replyQuestionDTO(reply.getId(), reply.getUsers(), reply.getContent(), reply.getType() == true ? 1 : 0, reply.getAnonymus() == true ? 1 : 0, reply.getStatus() == true ? 1 : 0, reply.getImg(), reply.getCreateAt());
                    listrepQuestiondto.add(replydto);
                }
                for (Reply reply : listrepP) {
                    replyPostDTO replydto = new replyPostDTO(reply.getId(), reply.getUsers(), reply.getContent(), reply.getType() == true ? 1 : 0, reply.getAnonymus() == true ? 1 : 0, reply.getStatus() == true ? 1 : 0, reply.getImg(), reply.getCreateAt());
                    listrepPostdtode.add(replydto);
                }
                postqeDTO dto = getPostById(listq.getPosts().getId());
                if (dto != null) {
                    postqeDTONotUser dtoNotUser = new postqeDTONotUser(dto.getId(), dto.getType(), dto.getAnonymus(),
                            dto.getDisplay_status(), dto.getListVote());
                    questionDTO = new questionDTO(listq.getId(), listq.getContent(), listq.getPoint(), listq.getImg(),
                            listq.getTitle(), listq.getCreateAt(), dto.getUsersPost(), dtoNotUser, listrepQuestiondto,
                            listrepPostdtode, dto.getTag());
                }
            }
        }
        return questionDTO;
    }

    public questionv2DTO getByIdsss(Integer id, Integer user) {
        PageRequest page = PageRequest.of(0, 1);
        Users_Posts aBoolean = users_postsDAO.findAllbyId1(id, page);
        if (aBoolean == null ? aBoolean == null : aBoolean.getStatus() == true) {
            Question listq = QuestionDAO.findQ(id);
            questionv2DTO questionDTO = null;
            if (listq != null) {
                if(listq.getPosts().getDisplay_status()==true){
                    List<replyQuestionDTO> listrepQuestiondto = new ArrayList<>();
                    List<replyPostDTO> listrepPostdtode = new ArrayList<>();
                    List<Reply> listrep = replyDAO.find(listq.getId(), true);
//                    List<Reply> listrepP = replyDAO.find2(listq.getId(), false);
                    for (Reply reply : listrep) {
                        replyQuestionDTO replydto = new replyQuestionDTO(reply.getId(), reply.getUsers(), reply.getContent(), reply.getType() == true ? 1 : 0, reply.getAnonymus() == true ? 1 : 0, reply.getStatus() == true ? 1 : 0, reply.getImg(), reply.getCreateAt());
                        listrepQuestiondto.add(replydto);
                    }
//                    for (Reply reply : listrepP) {
//                        replyPostDTO replydto = new replyPostDTO(reply.getId(), reply.getUsers(), reply.getContent(), reply.getType() == true ? 1 : 0, reply.getAnonymus() == true ? 1 : 0, reply.getStatus() == true ? 1 : 0, reply.getImg(), reply.getCreateAt());
//                        listrepPostdtode.add(replydto);
//                    }
                    postqev2DTO dto = getPostCount(listq.getPosts().getId(), user);
                    if (dto != null) {
                        PostVote2DTO dtoNotUser = new PostVote2DTO(dto.getId(), dto.getType(), dto.getAnonymus(),
                                dto.getDisplay_status(), dto.getUsersPost(), dto.getCountLike(), dto.getCountDis(), dto.getStauts());
                        questionDTO = new questionv2DTO(listq.getId(), listq.getContent(), listq.getPoint(), listq.getImg(),
                                listq.getTitle(), listq.getCreateAt(), dto.getUsersPost(), dtoNotUser, listrepQuestiondto,
                                listrepPostdtode, dto.getTag());
                    }
                }
                return questionDTO;
            }
            return null;
        } else {
            return null;
        }

    }

    public postqeDTO getPostById(Integer id) {
        Posts p = postDAO.getById(id);
        postqeDTO pDTO = null;
        if (p != null) {
            List<PostVoteDTO> listpv = new ArrayList<>();
            List<post_vote> pv = post_voteDAO.find(p.getId());
            List<Subject> sub = subjectDAO.findNew(p.getId());
            List<TagDTO> listtaDtos = new ArrayList<>();
            if (sub.size() > 0) {
                for (post_vote postv : pv) {
                    listpv.add(new PostVoteDTO(postv.getUsers(), postv.getLikes() == true ? 1 : 0, postv.getDislike() == true ? 1 : 0));
                }
                for (Subject sub1 : sub) {
                    listtaDtos.add(new TagDTO(sub1.getId(), sub1.getName()));
                }
                pDTO = new postqeDTO(p.getId(), p.getType() == true ? 1 : 0, p.getAnonymus() == true ? 1 : 0, p.getDisplay_status() == true ? 1 : 0, p.getUsers(),
                        listpv, listtaDtos);
            }
        }
        return pDTO;
    }

    public postqev2DTO getPostCount(Integer id, Integer user) {
        Posts p = postDAO.getById(id);
        postqev2DTO pDTO = null;
        if (p != null) {
            post_vote pv = post_voteDAO.findUser(p.getId(), user);
            List<Subject> sub = subjectDAO.findNew(p.getId());
            List<TagDTO> listtaDtos = new ArrayList<>();
            int staus = 0;
            if (pv != null) {
                if (pv.getLikes() == true) {
                    staus = 1;
                } else {
                    staus = 2;
                }
            }
            if (sub.size() > 0) {
                for (Subject sub1 : sub) {
                    listtaDtos.add(new TagDTO(sub1.getId(), sub1.getName()));
                }
                pDTO = new postqev2DTO(p.getId(), p.getType() == true ? 1 : 0, p.getAnonymus() == true ? 1 : 0, p.getDisplay_status() == true ? 1 : 0, p.getUsers(), listtaDtos, post_voteDAO.CountLike(p.getId()), post_voteDAO.CountDis(p.getId()), staus);
            }
        }
        return pDTO;
    }
}
