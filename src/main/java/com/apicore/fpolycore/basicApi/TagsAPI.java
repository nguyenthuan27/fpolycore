package com.apicore.fpolycore.basicApi;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.apicore.fpolycore.Class.Subject;
import com.apicore.fpolycore.Class.Tags;
import com.apicore.fpolycore.DAO.PostDAO;
import com.apicore.fpolycore.DAO.SubjectDAO;
import com.apicore.fpolycore.DAO.TagsDAO;
import com.apicore.fpolycore.DTO.subjectDTO;
import com.apicore.fpolycore.DTO.tagsInfoDTO;


@CrossOrigin("*")
@RestController
@RequestMapping("tags")
public class TagsAPI {
    @Autowired
    TagsDAO TagsDAO;
    @Autowired
    SubjectDAO subjectDAO;
    @Autowired
    PostDAO postDAO;

    @GetMapping("/get")
    public ResponseEntity<List<tagsInfoDTO>> getfull() {
        return ResponseEntity.ok(getList());
    }

    @GetMapping("/get/{x}")
    public ResponseEntity<tagsInfoDTO> getOne(@PathVariable("x") Integer id) {
        return ResponseEntity.ok(getByid(id));
    }

    @PostMapping("/post")
    public ResponseEntity<Tags> post(@RequestBody Tags tags, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.badRequest().build();
        } else {
            if (TagsDAO.existsById(tags.getId())) {
                return ResponseEntity.badRequest().build();
            }
            TagsDAO.save(tags);
            return ResponseEntity.ok(tags);
        }
    }

    @PutMapping("/put/{x}")
    public ResponseEntity<Tags> put(@PathVariable("x") Integer id, @RequestBody Tags tags, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.badRequest().build();
        } else {
            if (TagsDAO.existsById(tags.getId()) && id == tags.getId()) {
                TagsDAO.save(tags);
                return ResponseEntity.ok(tags);
            }
            return ResponseEntity.badRequest().build();
        }
    }

    @DeleteMapping("/delete/{x}")
    public ResponseEntity<Void> delete(@PathVariable("x") Integer id) {
        if (!TagsDAO.existsById(id)) {
            return ResponseEntity.ok().build();
        }
        TagsDAO.deleteById(id);
        return ResponseEntity.ok().build();
    }

    public List<tagsInfoDTO> getList() {
        List<Tags> listTags = TagsDAO.findAll();
        List<tagsInfoDTO> list = new ArrayList<>();
        if (listTags != null) {
            for (Tags tag : listTags) {
                subjectDTO subdto = new subjectDTO();
                tagsInfoDTO tagsdto = new tagsInfoDTO();

                Subject sub = subjectDAO.find(tag.getId());
                if (sub != null) {
                    subdto = new subjectDTO(sub.getId(), sub.getName(), sub.getSmall_point());
                }

                tagsdto.setId(tag.getId());
                tagsdto.setSubject(subdto);
                list.add(tagsdto);
            }
        }
        return list;

    }

    public tagsInfoDTO getByid(Integer id) {
        Tags tag = TagsDAO.findOne(id);
        tagsInfoDTO tagsdto = new tagsInfoDTO();
        if (tag != null) {
            subjectDTO subdto = new subjectDTO();
            Subject sub = subjectDAO.find(tag.getId());
            if (sub != null) {
                subdto = new subjectDTO(sub.getId(), sub.getName(), sub.getSmall_point());
            }
            tagsdto.setId(tag.getId());
            tagsdto.setSubject(subdto);
        }
        return tagsdto;
    }
}
