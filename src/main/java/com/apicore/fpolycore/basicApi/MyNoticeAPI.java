package com.apicore.fpolycore.basicApi;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.apicore.fpolycore.Class.Reply;
import com.apicore.fpolycore.DAO.ReplyDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.apicore.fpolycore.Class.notification;
import com.apicore.fpolycore.DAO.MyNoticeDAO;
import com.apicore.fpolycore.DTO.notificationDTO;


@CrossOrigin("*")
@RestController
    @RequestMapping("notification")
public class MyNoticeAPI {
    @Autowired
    MyNoticeDAO MyNoticeDAO;
    @Autowired
    ReplyDAO replyDAO;

    @GetMapping("/get")
    public ResponseEntity<List<notification>> getfull() {
        return ResponseEntity.ok(MyNoticeDAO.findAll());
    }

    @PostMapping("/post")
    public ResponseEntity<notification> post(@RequestBody notification notice, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.badRequest().build();
        } else {
            if (MyNoticeDAO.existsById(notice.getId())) {
                return ResponseEntity.badRequest().build();
            }
            MyNoticeDAO.save(notice);
            return ResponseEntity.ok(notice);
        }

    }

    @PutMapping("/put/{x}")
    public ResponseEntity<notification> put(@PathVariable("x") Integer id, @RequestBody notification notice, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.badRequest().build();
        } else {
            if (MyNoticeDAO.existsById(notice.getId()) && id == notice.getId()) {
                MyNoticeDAO.save(notice);
                return ResponseEntity.ok(notice);
            }
            return ResponseEntity.badRequest().build();
        }
    }

    @DeleteMapping("/delete/{x}")
    public ResponseEntity<Void> delete(@PathVariable("x") Integer id) {
        if (!MyNoticeDAO.existsById(id)) {
            return ResponseEntity.ok().build();
        }
        MyNoticeDAO.deleteById(id);
        return ResponseEntity.ok().build();
    }
    @GetMapping("/get/{x}")
    public ResponseEntity<List<notificationDTO>> getBt(@PathVariable("x") Integer id) {
        List<notificationDTO> list =new ArrayList<>();
        List<notification> data = MyNoticeDAO.findOne(id);
            if(data.size()>0){
                for(notification n:data){
                    notificationDTO dto  = new notificationDTO();
                    dto.setIdnotification(n.getId());
                    dto.setContent(n.getContent());
                    dto.setCreateAt(n.getCreateAt());
                    dto.setTitle(n.getTitle());
                    dto.setType(n.getType());
                    if(n.getTitle().contains("bình luận")){
                        String[] m_AppVersionS = n.getTitle().split("[:]");
                            Reply reply = replyDAO.findRs(Integer.valueOf(m_AppVersionS[1].replaceAll(" ", "")));
                            if(reply!=null){
                                dto.setIdreply(Integer.valueOf(m_AppVersionS[1].replaceAll(" ", "")));
                                dto.setIdpost(reply.getQuestion().getPosts().getId());
                            }
                    }
                    else if(n.getTitle().contains("bài viết")){
                        String[] m_AppVersionS = n.getTitle().split("[:]");
                        dto.setIdpost(Integer.valueOf(m_AppVersionS[1].replaceAll(" ", "")));
                        dto.setIdreply(0);
                    }
                    if(n.getTitle().equals("report")) {
                        dto.setReport(true);
                    }
                    else{
                        dto.setReport(false);
                    }
                    list.add(dto);
                }
            }

        return ResponseEntity.ok(list);
    }
    @GetMapping("/update/{x}")
    public ResponseEntity<Boolean> getupdate(@PathVariable("x") Integer id) {
        if (!MyNoticeDAO.existsById(id)) {
            return ResponseEntity.badRequest().build();
        }
        notification notification =MyNoticeDAO.findById(id).get();
        notification.setType(true);
        MyNoticeDAO.save(notification);
        return ResponseEntity.ok(true);
    }
}
