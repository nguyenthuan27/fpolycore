package com.apicore.fpolycore.Sercurity;

import java.util.ArrayList;
import java.util.List;

import java.util.Optional;
import java.util.stream.Collectors;

import com.apicore.fpolycore.Class.Posts;
import com.apicore.fpolycore.Class.Question;
import com.apicore.fpolycore.Class.Users;
import com.apicore.fpolycore.DAO.UsersDAO;
import com.apicore.fpolycore.DTO.RoleDTO;
import com.apicore.fpolycore.DTO.postOfUserDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Service
@CrossOrigin("*")
@RestController
@RequestMapping("role")
public class UserService implements UserDetailsService {
	@Autowired
	UsersDAO account;
	private String keyss = "1";

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		Optional<Users> user = account.findemail1(email);
		List<String> anh = getRole(user.get().getRole());
		List<GrantedAuthority> authorities = anh.stream().map(a -> "ROLE_" + a)
				.map(menuName -> new SimpleGrantedAuthority(menuName)).collect(Collectors.toList());
		User dl = new User(user.get().getEmail(), keyss, authorities);
		return dl;

	}

	public List<String> getRole(String role) {
		List<String> list = new ArrayList<>();
		if (role.length() == 1) {
			if (getRoleString(role) != null) {
				list.add(getRoleString(role));
			}
		} else {
			String[] parts = role.split("-");
			for (String s : parts) {
				if (getRoleString(s) != null) {
					list.add(getRoleString(s));
				}
			}
		}
		return list;

	}

	public String getRoleString(String role) {
		if (role.equals("1")) {
			return "SV";// sinh vieen
		} else if (role.equals("2")) {
			return "GV";// giao vien
		} else if (role.equals("3")) {
			return "ADMIN";// ADMIN
		}else if (role.equals("3")) {
			return "MOD";// ADMIN
		}
		return null;
	}

	public String setRoleString(String role) {
		if (role.equals("SV")) {
			return "1";// sinh vieen
		} else if (role.equals("GV")) {
			return "2";// giao vien
		} else if (role.equals("ADMIN")) {
			return "3";// ADMIN
		}else if (role.equals("MOD")) {
			return "4";// ADMIN
		}
		return null;
	}

	public UserDetails loadUserByUsername1(String email, String key) {
		keyss = key;
		return loadUserByUsername(email);
	}

	@GetMapping("/getListRole")
	public ResponseEntity<List<RoleDTO>> getListRoles() {
		return ResponseEntity.ok(getListRole());
	}

	@GetMapping("/getRole/{x}")
	public ResponseEntity<RoleDTO> getRoles(@PathVariable("x") Integer id) {
		return ResponseEntity.ok(getRole(id));
	}

	@PutMapping("/put/{x}")
	public ResponseEntity<String> put(@PathVariable("x") Integer id, @Validated @RequestBody RoleDTO role,
			BindingResult result) {
		if (result.hasErrors()) {
			return ResponseEntity.badRequest().build();
		} else {
			if (account.existsById(role.getIdUser()) && id == role.getIdUser()) {
				Users users = account.getById(role.getIdUser());
//            	users.setRole(setRoleString(role.getRole()));
				if (role.getRole().size() > 0) {
					List<Integer> sx = new ArrayList<>();
					String b = null;
					for (String a : role.getRole()) {
						String data = setRoleString(a);
						if (data != null) {
							try {
								sx.add(Integer.valueOf(data));
							} catch (Exception e) {
								return ResponseEntity.badRequest().build();
							}
							if (b == null) {
								b = data;
							} else {
								b += "-" + data;
							}
						}

					}
					sx.sort((o1, o2) -> o1 - o2);
					System.out.println(sx);
					
					if (sx.size() == 1) {
						users.setRole(String.valueOf(sx.get(0)));
					} else if (sx.size() == 2) {
						users.setRole(String.valueOf(sx.get(0) + "-" + sx.get(1)));
					} else if (sx.size() == 3) {
						users.setRole(String.valueOf(sx.get(0) + "-" + sx.get(1) + "-" + sx.get(2)));
					}

					account.save(users);
					return ResponseEntity.ok("ok");
				} else {
					return ResponseEntity.badRequest().build();
				}

			}
			return ResponseEntity.badRequest().build();
		}

	}

	public List<RoleDTO> getListRole() {
		List<RoleDTO> listRole = new ArrayList<RoleDTO>();
		List<Users> listu = account.findAll();

    	System.out.println(java.time.LocalDate.now());
		for (Users ul : listu) {
			RoleDTO udto = new RoleDTO();
			String role = ul.getRole();
			List<String> lrole = new ArrayList<String>();
			if (role != null) {
				udto.setIdUser(ul.getId());
				udto.setName(ul.getFullname());
			
			if (role.contains("1")) {
				lrole.add("Sinh Viên");
				udto.setRole(lrole);
			} if(role.contains("2")) {
				lrole.add("Giảng Viên");
				udto.setRole(lrole);
			} if(role.contains("3")) {
				lrole.add("ADMIN");
				udto.setRole(lrole);
			}
			}			
			listRole.add(udto);
		}
		return listRole;

	}

	public RoleDTO getRole(Integer id) {
		Users u = account.getById(id);

		RoleDTO udto = new RoleDTO();
		udto.setIdUser(u.getId());
		udto.setName(u.getFullname());
		String role = u.getRole();
		List<String> lrole = new ArrayList<String>();
		if (role != null) {
			if (role.contains("1")) {
				lrole.add("Sinh Viên");
				udto.setRole(lrole);
			}
			if (role.contains("2")) {
				lrole.add("Giảng Viên");
				udto.setRole(lrole);
			}
			if (role.contains("3")) {
				lrole.add("ADMIN");
				udto.setRole(lrole);
			}

		}
		return udto;

	}
		
}
