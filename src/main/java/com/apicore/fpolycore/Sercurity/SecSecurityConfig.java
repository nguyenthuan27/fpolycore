package com.apicore.fpolycore.Sercurity;

import com.apicore.fpolycore.DAO.UsersDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;

import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    UserService userServie;
    @Autowired
    private JwtFilter JwtFilter;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userServie);
    }

    @Override
    @Bean
    protected AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable().authorizeRequests()
//                .antMatchers("/Login").hasRole("SV")
                .antMatchers("/Login").permitAll()
                .antMatchers("/question/**").permitAll()
                .antMatchers("/post/**").permitAll()
                .antMatchers("/user/**").permitAll()
                .antMatchers("/subject/**").permitAll()
                .antMatchers("/pointHistory/**").permitAll()
                .antMatchers("/role/**").permitAll()
//                .antMatchers("/sid/**").permitAll()
//                .antMatchers("/accounts/**").hasAnyRole("DIRE", "STAF")
//                .antMatchers("/Category/**").hasAnyRole("DIRE", "STAF")
//                .antMatchers("/Order/**").hasAnyRole("DIRE", "STAF")
//                .antMatchers("/OrderDetail/**").hasAnyRole("DIRE", "STAF")
//                .antMatchers("/Product/**").hasAnyRole("DIRE", "STAF")
//                .antMatchers("/upload").hasAnyRole("DIRE", "STAF")
//                .antMatchers("/Role/**").hasAuthority("ROLE_DIRE")
//                .antMatchers("/authority/**").hasAuthority("ROLE_DIRE")
                .and().addFilterBefore(JwtFilter, UsernamePasswordAuthenticationFilter.class);
    }

}
