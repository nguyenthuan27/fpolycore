package com.apicore.fpolycore.DTO;

import java.util.List;

import com.apicore.fpolycore.Class.Question;
import com.apicore.fpolycore.Class.Reply;
import com.apicore.fpolycore.Class.Reports;
import com.apicore.fpolycore.Class.Users;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdminReportDTO {
    private Reports report;
    private Boolean status;
    private Question question;
    private Reply reply;
}
