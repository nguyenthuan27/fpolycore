package com.apicore.fpolycore.DTO;

import java.sql.Date;
import java.sql.Timestamp;

import com.apicore.fpolycore.Class.Users;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ListUser_Report {
    private Integer id;
    @JsonFormat(pattern="YYYY/MM/dd HH:mm:ss")
    private Timestamp createAt;
    private Boolean status;
    private Users users;
}
