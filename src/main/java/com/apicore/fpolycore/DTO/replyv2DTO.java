package com.apicore.fpolycore.DTO;

import com.apicore.fpolycore.Class.Users;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class replyv2DTO {
    private Integer idreply;
    private Integer idposts;
    private String content;
    private Integer type;
    private Integer anonymus;
    private Integer status;
    private String img;
    private Date createAt;
    private Integer like;
    private Integer dislike;
}
