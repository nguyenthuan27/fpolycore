package com.apicore.fpolycore.DTO;

import java.util.List;

import com.apicore.fpolycore.Class.Reports;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class reportQuestionDTO {
    private Integer id;
    private Reports report;
    private questionVoteDTO question;
    private ListUser_Report userReport;
}
