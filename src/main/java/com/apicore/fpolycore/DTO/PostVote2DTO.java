package com.apicore.fpolycore.DTO;

import java.util.List;

import com.apicore.fpolycore.Class.Users;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PostVote2DTO {
    private Integer id;
    private Integer type;
    private Integer anonymus;
    private Integer display_status;
    private Users UsersPost;
    private Integer countLike;
    private Integer countDis;
    private Integer stauts;
}
