package com.apicore.fpolycore.DTO;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.apicore.fpolycore.Class.Users;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class StatisticalCount implements Serializable {
    @Id
    private Users users;
    private Long count;

}
