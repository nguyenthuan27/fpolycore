package com.apicore.fpolycore.DTO;

import com.apicore.fpolycore.Class.Reports;
import com.apicore.fpolycore.Class.Users;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO {
    private Users users;
    private Reports report;
//    private boolean like;
//    private boolean dislike;
}
