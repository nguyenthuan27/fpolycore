package com.apicore.fpolycore.DTO;

import java.sql.Timestamp;

import com.apicore.fpolycore.Class.Users;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PointHistoryDTO {
    private Users user;
    private Integer point;
    @JsonFormat(pattern="YYYY/MM/dd HH:mm:ss")
    private Timestamp createDate;
    private String type;
    private String note;
}
