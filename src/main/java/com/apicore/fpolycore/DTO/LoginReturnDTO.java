package com.apicore.fpolycore.DTO;

import com.apicore.fpolycore.Class.Users;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginReturnDTO {
    private String token;
    private Collection<? extends GrantedAuthority> role;
    private Users user;
    private Boolean newuser;
}
