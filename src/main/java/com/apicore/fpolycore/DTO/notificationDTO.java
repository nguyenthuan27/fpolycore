package com.apicore.fpolycore.DTO;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class notificationDTO {
    private Integer idnotification;
    private String title;
    private String content;
    private Boolean type;
    @JsonFormat(pattern="YYYY/MM/dd HH:mm:ss")
    private Timestamp createAt;
    private Integer idpost;
    private Integer idreply;
    private Boolean report;
}
