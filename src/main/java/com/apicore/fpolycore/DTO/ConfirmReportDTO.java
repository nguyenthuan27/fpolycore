package com.apicore.fpolycore.DTO;

import java.util.List;

import com.apicore.fpolycore.Class.Reports;
import com.apicore.fpolycore.Class.Users;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ConfirmReportDTO {
    private Boolean check;
    private Integer confirmUser;
    private Integer reportId;
    private Boolean status;
}
