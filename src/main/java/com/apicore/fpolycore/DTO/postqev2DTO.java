package com.apicore.fpolycore.DTO;

import com.apicore.fpolycore.Class.Users;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class postqev2DTO {
    private Integer id;
    private Integer type;
    private Integer anonymus;
    private Integer display_status;
    private Users UsersPost;
    private List<TagDTO> tag;
    private Integer countLike;
    private Integer countDis;
    private Integer stauts;

}