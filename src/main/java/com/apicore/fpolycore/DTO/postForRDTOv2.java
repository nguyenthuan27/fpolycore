package com.apicore.fpolycore.DTO;

import com.apicore.fpolycore.Class.Users;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class postForRDTOv2 {
    private Integer id;
    private Integer type;
    private Integer status;
    private Integer anonymus;
    private Integer display_status;
    private questionVoteDTO question;
    private Users UsersPost;
    private int Like;
    private int DisLike;
    private int Comment;
    private int DetailedComment;
    private List<TagDTO> tag;
}