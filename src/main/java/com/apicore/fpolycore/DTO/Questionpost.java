package com.apicore.fpolycore.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Questionpost {
    private String content;
    private Integer point;
    private String img;
    private String title;
    private Integer idpost;
    private List<TagDTO> tag;
}
