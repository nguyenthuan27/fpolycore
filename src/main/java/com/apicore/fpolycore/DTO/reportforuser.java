package com.apicore.fpolycore.DTO;

import com.apicore.fpolycore.Class.Question;
import com.apicore.fpolycore.Class.Reply;
import com.apicore.fpolycore.Class.Users;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class reportforuser {
    private int idUser;
    private int idReport;
    private String Content;
    private Boolean type;
}

