package com.apicore.fpolycore.DTO;

import com.apicore.fpolycore.Class.ReportQuestion;
import com.apicore.fpolycore.Class.ReportReply;
import com.apicore.fpolycore.Class.Users;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@SuppressWarnings("serial")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReportsquestionDTO {
    private int idusers;
    private String content;
    private int question;
}
