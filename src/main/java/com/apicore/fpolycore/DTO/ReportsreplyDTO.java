package com.apicore.fpolycore.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@SuppressWarnings("serial")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReportsreplyDTO {
    private int idusers;
    private String content;
    private int reply;
}
