package com.apicore.fpolycore.DTO;

import java.util.Date;

import com.apicore.fpolycore.Class.Users;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class replyPostDTO {
    private Integer id;
    private Users user;
    private String content;
    private Integer type;
    private Integer anonymus;
    private Integer status;
    private String img;
    private Date createAt;
}
