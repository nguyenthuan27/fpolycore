package com.apicore.fpolycore.DTO;

import com.apicore.fpolycore.Class.Users;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Timer;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class questionv2sDTO {
    private Integer idqe;
    private Integer idposts;
    private String tilte;
    private String content;
    private Integer type;
    private Integer anonymus;
    private Integer status;
    private String img;
    @JsonFormat(pattern="YYYY/MM/dd HH:mm:ss")
    private Timestamp createAt;
    private Integer like;
    private Integer dislike;
    private Integer point;
    private List<TagDTO> tag;
}
