package com.apicore.fpolycore.DTO;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import com.apicore.fpolycore.Class.Users;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class blogContentDTO {
    private Integer id;
    private String content;
    private String img;
    private String title;
//    private List<blogTagDTO> tag;
}
