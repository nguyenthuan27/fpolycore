package com.apicore.fpolycore.DTO;

import com.apicore.fpolycore.Class.Branches;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class userProfileDTO {
    private String fullname;
    private Integer branchId;
    private String image;
}
