package com.apicore.fpolycore.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class tagsInfoDTO {
    private Integer id;
    private subjectDTO subject;
}
