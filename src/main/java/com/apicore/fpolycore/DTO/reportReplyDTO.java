package com.apicore.fpolycore.DTO;

import com.apicore.fpolycore.Class.Reports;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class reportReplyDTO {
    private Integer id;
    private Reports report;
    private replyPostDTO replyPost;
    private ListUser_Report userReport;
}
