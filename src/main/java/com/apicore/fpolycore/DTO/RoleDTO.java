package com.apicore.fpolycore.DTO;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoleDTO {
    private int idUser;
    private String name;
//    private String role;
    private List<String> role;
}

