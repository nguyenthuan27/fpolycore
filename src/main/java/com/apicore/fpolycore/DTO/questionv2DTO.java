package com.apicore.fpolycore.DTO;

import com.apicore.fpolycore.Class.Users;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class questionv2DTO {
    private Integer id;
    private String content;
    private Integer point;
    private String img;
    private String title;
    @JsonFormat(pattern="YYYY/MM/dd HH:mm:ss")
    private Timestamp  updateDay;
    private Users UsersPost;
    private PostVote2DTO post;
    private List<replyQuestionDTO> listrComment;
    private List<replyPostDTO> listrDetailedComment;
    private List<TagDTO> tag;
}
