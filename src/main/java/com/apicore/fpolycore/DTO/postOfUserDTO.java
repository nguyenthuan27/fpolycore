package com.apicore.fpolycore.DTO;

import java.sql.Date;
import java.sql.Timestamp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class postOfUserDTO {
    private Integer id;
    private Integer point;
    private Timestamp createAt;
    private String title;

}
