package com.apicore.fpolycore.DTO;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import com.apicore.fpolycore.Class.Users;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class questionDTO {
    private Integer id;
    private String content;
    private Integer point;
    private String img;
    private String title;
    @JsonFormat(pattern="YYYY/MM/dd HH:mm:ss")
    private Timestamp  updateDay;
    private Users UsersPost;
    private postqeDTONotUser post;
    private List<replyQuestionDTO> listrComment;
    private List<replyPostDTO> listrDetailedComment;
    private List<TagDTO> tag;
}
