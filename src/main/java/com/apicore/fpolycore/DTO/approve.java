package com.apicore.fpolycore.DTO;

import com.apicore.fpolycore.Class.Reports;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class approve {
    private int idUser;
    private int idapprove;
    private Boolean status;
}

