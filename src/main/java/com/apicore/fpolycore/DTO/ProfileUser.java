package com.apicore.fpolycore.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProfileUser {
    private List<replyv2DTO> listrComment;
    private List<questionv2sDTO> listrDetailedComment;
}
