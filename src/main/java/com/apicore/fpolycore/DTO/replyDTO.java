package com.apicore.fpolycore.DTO;

import java.util.List;

import com.apicore.fpolycore.Class.Reply;
import com.apicore.fpolycore.Class.Reply_Vote;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class replyDTO {
    private Reply reply;
    private List<Reply_Vote> reply_vote;
}
