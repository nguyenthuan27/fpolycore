package com.apicore.fpolycore.DTO;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import com.apicore.fpolycore.Class.Posts;
import com.apicore.fpolycore.Class.Question;
import com.apicore.fpolycore.Class.Reply;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class questionVoteDTO {
    private Integer id;
    private String content;
    private Integer point;
    private String img;
    private String title;
    @JsonFormat(pattern="YYYY/MM/dd HH:mm:ss")
    private Timestamp  updateDay;
    @JsonFormat(pattern="YYYY/MM/dd HH:mm:ss")
    private Timestamp  createDay;
}
