package com.apicore.fpolycore.DTO;

import java.sql.Timestamp;
import java.util.List;

import javax.validation.constraints.NotNull;

import com.apicore.fpolycore.Class.post_vote;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.apicore.fpolycore.Class.Question;
import com.apicore.fpolycore.Class.Users;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class blogDTO {
    private Integer id;
    private Boolean type;
    private Boolean anonymus;
    private Boolean display_status;
    private blogContentDTO content;
    private Integer status;
    private Integer userId;
    private List<PostVoteDTO> ListVote;
    private List<blogTagDTO> tag;
}