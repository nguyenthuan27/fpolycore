package com.apicore.fpolycore.DTO;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.apicore.fpolycore.Class.post_vote;
import com.apicore.fpolycore.Class.Question;
import com.apicore.fpolycore.Class.Users;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class postqeDTONotUser {
    private Integer id;
    private Integer type;
    private Integer anonymus;
    private Integer display_status;
    private List<PostVoteDTO> ListVote;
}