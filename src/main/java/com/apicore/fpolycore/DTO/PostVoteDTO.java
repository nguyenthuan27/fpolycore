package com.apicore.fpolycore.DTO;

import java.util.List;

import com.apicore.fpolycore.Class.post_vote;
import com.apicore.fpolycore.Class.Users;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PostVoteDTO {
    private Users user;
    private Integer like;
    private Integer dislike;
}
