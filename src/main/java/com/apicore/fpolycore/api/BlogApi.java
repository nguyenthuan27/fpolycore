package com.apicore.fpolycore.api;


import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.apicore.fpolycore.Class.*;
import com.apicore.fpolycore.DAO.*;
import com.apicore.fpolycore.DTO.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin("*")
@RestController
@RequestMapping("blog")
public class BlogApi {
    @Autowired
    QuestionDAO questionDAO;
    @Autowired
    PostDAO postDAO;
    @Autowired
    UsersDAO usersDAO;
    @Autowired
    Post_VoteDAO post_voteDAO;
    @Autowired
    ReplyDAO replyDAO;
    @Autowired
    SubjectDAO subjectDAO;
    @Autowired
    Users_PostsDAO users_postsDAO;
    @Autowired
    TagsDAO tagsDAO;
    
    @GetMapping("/getAll")
    public ResponseEntity<List<postForRDTOv2>> getfull() {
        return ResponseEntity.ok(getList());
    }

    @GetMapping("/getById/{x}")
    public ResponseEntity<postForRDTO> get(@PathVariable("x") Integer id) {
        return ResponseEntity.ok(getPostById(id));
    }

    @PostMapping("/post")
	public ResponseEntity<String> post(@RequestBody blogDTO blog, BindingResult result) {
    	if (result.hasErrors()) {
			return ResponseEntity.badRequest().build();
		} else {
			if (postDAO.existsById(blog.getId())) {
				return ResponseEntity.badRequest().build();
			}
	    	Date now = new Date();
			Timestamp timestamp = new Timestamp(now.getTime());
			Posts post = new Posts();
			Users users = usersDAO.getById(blog.getUserId());
			if(users!=null) {
				post.setUsers(users);
			post.setAnonymus(blog.getAnonymus());
			post.setDisplay_status(blog.getDisplay_status());
			post.setType(blog.getType());
			post.setCreateAt(timestamp);
			post.setStatus(blog.getStatus());
			postDAO.save(post);
			
			for(blogTagDTO tags:blog.getTag()){
                Tags tags1 =new Tags();
                tags1.setPosts_id(post.getId());
                tags1.setSubject_id(tags.getId());
                tagsDAO.save(tags1);
            }
			
			Question question = new Question();
			question.setContent(blog.getContent().getContent());
			question.setPosts(post);
			question.setImg(blog.getContent().getImg());
			question.setCreateAt(timestamp);
			question.setTitle(blog.getContent().getTitle());
			questionDAO.save(question);
			}
			
			
			return ResponseEntity.ok("Tạo blog thành công!");
		}

}

    @PutMapping("/put/{x}")
    public ResponseEntity<Question> put(@PathVariable("x") String id, @RequestBody Question question) {
        return ResponseEntity.ok(question);
    }

    @DeleteMapping("/delete/{x}")
    public ResponseEntity<Void> delete(@PathVariable("x") String id) {
        return ResponseEntity.ok().build();
    }

    public List<postForRDTOv2> getList() {
        List<Posts> listPost = postDAO.findBL();
        List<postForRDTOv2> list = new ArrayList<postForRDTOv2>();
        if (listPost != null) {
            for (Posts dl : listPost) {
                PageRequest page = PageRequest.of(0, 1);
                Users_Posts aBoolean = users_postsDAO.findAllbyId1(dl.getId(), page);
                if (aBoolean == null ? aBoolean == null : aBoolean.getStatus() == false) {
                    if (dl.getDisplay_status() == true && dl.getType()==false) {
                        Question questionDAOOne = questionDAO.findQ(dl.getId());
                        if (questionDAOOne != null) {
                            List<PostVoteDTO> listpv = new ArrayList<>();
                            questionVoteDTO qdto = new questionVoteDTO();
                            Question dateold = questionDAO.findNew(dl.getId());
                            List<post_vote> pv = post_voteDAO.find(dl.getId());
                            qdto = new questionVoteDTO(questionDAOOne.getId(), questionDAOOne.getContent(), questionDAOOne.getPoint(), questionDAOOne.getImg(), questionDAOOne.getTitle(), questionDAOOne.getCreateAt(), dateold.getCreateAt());
                            for (post_vote postv : pv) {
                                listpv.add(new PostVoteDTO(postv.getUsers(), postv.getLikes() == true ? 1 : 0, postv.getDislike() == true ? 1 : 0));
                            }
                            List<TagDTO> listtaDtos = new ArrayList<>();
                            List<Subject> sub12 = subjectDAO.findNew(dl.getId());
                            List<Reply> listrep = replyDAO.find(questionDAOOne.getId(), true);
                            List<Reply> listrepP = replyDAO.find2(questionDAOOne.getId(), false);
                            if (sub12.size() > 0) {
                                for (Subject sub1 : sub12) {
                                    listtaDtos.add(new TagDTO(sub1.getId(), sub1.getName()));
                                }
                                postForRDTOv2 pDTO = new postForRDTOv2(dl.getId(), dl.getType() == true ? 1 : 0, dl.getStatus(), dl.getAnonymus() == true ? 1 : 0, dl.getDisplay_status() == true ? 1 : 0, qdto, dl.getUsers(), post_voteDAO.CountDis(dl.getId()), post_voteDAO.CountLike(dl.getId()), listrep.size(), listrepP.size(), listtaDtos);
                                list.add(pDTO);
                            }
                        }
                    }

                }
            }
        }
        return list;
    }

    public postForRDTO getPostById(Integer id) {
        Posts p = postDAO.findOne(id);
        postForRDTO pDTO = new postForRDTO();
        Question q = questionDAO.findOne(p.getId());
        if (q != null && p != null) {
            Question dateold = questionDAO.findNew(p.getId());
            questionVoteDTO qdto = new questionVoteDTO(q.getId(), q.getContent(), q.getPoint(), q.getImg(), q.getTitle(), q.getCreateAt(), dateold.getCreateAt());
            List<TagDTO> listtaDtos = new ArrayList<>();
            List<Subject> sub = subjectDAO.findNew(p.getId());
            List<replyPostDTO> listrepPostdtode = new ArrayList<>();
            if (sub.size() > 0) {
                for (Subject sub1 : sub) {
                    listtaDtos.add(new TagDTO(sub1.getId(), sub1.getName()));
                }
                List<Reply> listrepP = replyDAO.find2(q.getId(), true);
                for (Reply reply : listrepP) {
                    replyPostDTO replydto = new replyPostDTO(reply.getId(), reply.getUsers(), reply.getContent(), reply.getType() == true ? 1 : 0, reply.getAnonymus() == true ? 1 : 0, reply.getStatus() == true ? 1 : 0, reply.getImg(), reply.getCreateAt());
                    listrepPostdtode.add(replydto);
                }
                pDTO = new postForRDTO(p.getId(), p.getType() == true ? 1 : 0, p.getAnonymus() == true ? 1 : 0, p.getDisplay_status() == true ? 1 : 0, qdto, p.getUsers(), post_voteDAO.CountDis(id), post_voteDAO.CountLike(id), questionDAO.Count(id), listrepPostdtode, listtaDtos);
            }
        }
        return pDTO;
    }
}

