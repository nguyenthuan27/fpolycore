package com.apicore.fpolycore.api;

import java.sql.Timestamp;
import java.util.*;

import com.apicore.fpolycore.DTO.CountMonth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.apicore.fpolycore.Class.PointHistory;
import com.apicore.fpolycore.Class.Reply;
import com.apicore.fpolycore.Class.Users;
import com.apicore.fpolycore.Class.notification;
import com.apicore.fpolycore.DTO.replyDTO;
import com.apicore.fpolycore.DAO.Reply_VoteDAO;
import com.apicore.fpolycore.DAO.UsersDAO;
import com.apicore.fpolycore.DAO.MyNoticeDAO;
import com.apicore.fpolycore.DAO.PointHistoryDAO;
import com.apicore.fpolycore.DAO.QuestionDAO;
import com.apicore.fpolycore.DAO.ReplyDAO;

@CrossOrigin("*")
@RestController
    @RequestMapping("reply")
public class ReplyApi {
    @Autowired
    ReplyDAO replyDAO;
    @Autowired
    UsersDAO usersDAO;
    @Autowired
    Reply_VoteDAO reply_voteDAO;
    @Autowired
    QuestionDAO quetionDAO;
	@Autowired
	PointHistoryDAO pointHistoryDAO;
	@Autowired
	MyNoticeDAO notificationDAO;


    @GetMapping("/get")
    public ResponseEntity<List<replyDTO>> getfull() {
        return ResponseEntity.ok(getList());
    }
    @GetMapping("/get1")
    public ResponseEntity<List<Reply>> getfull1() {
        return ResponseEntity.ok(replyDAO.findAll());
    }

    @GetMapping("/get/{x}")
    public ResponseEntity<Optional<Reply>> get(@PathVariable("x") Integer id) {
        return ResponseEntity.ok(replyDAO.findById(id));
    }

    @PostMapping("/post")
    public ResponseEntity<Reply> post(@RequestBody Reply reply, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.badRequest().build();
        } else {
            if (replyDAO.existsById(reply.getId())) {
                return ResponseEntity.badRequest().build();
            }
            replyDAO.save(reply);

			Date now = new Date();
			Timestamp timestamp = new Timestamp(now.getTime());
			Users user = usersDAO.getById(reply.getUsers().getId());
			int point = (reply.getQuestion().getPoint() * 50 / 100);
			user.setPoint(user.getPoint() + point);
			usersDAO.save(user);

            PointHistory ph = new PointHistory();
			ph.setUsers(reply.getUsers());
			ph.setPoint(user.getPoint());
			ph.setCreate_date(timestamp);
			ph.setType("+" + point);
			ph.setNote("reply:" + reply.getId());
			pointHistoryDAO.save(ph);

			notification noti = new notification();
			noti.setContent("Bạn đã được cộng " + point + " khi trả lời bài viết");
			noti.setCreateAt(timestamp);
			noti.setTitle("Cộng điểm bình luận: " + reply.getId());
			noti.setType(true);
			noti.setUser_id(reply.getUsers().getId());
			notificationDAO.save(noti);
            return ResponseEntity.ok(reply);
        }

    }

    @PutMapping("/put/{x}")
    public ResponseEntity<Reply> put(@PathVariable("x") Integer id, @RequestBody Reply reply, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.badRequest().build();
        } else {
            if (replyDAO.existsById(reply.getId()) && id == reply.getId()) {
                replyDAO.save(reply);
                return ResponseEntity.ok(reply);
            }
            return ResponseEntity.badRequest().build();
        }
    }

    @DeleteMapping("/delete/{x}")
    public ResponseEntity<Void> delete(@PathVariable("x") Integer id) {
        if (!replyDAO.existsById(id)) {
            return ResponseEntity.ok().build();
        }
        replyDAO.deleteById(id);
        return ResponseEntity.ok().build();
    }

    private List<replyDTO> getList() {
        List<replyDTO> list = new ArrayList<replyDTO>();
        List<Reply> listr = replyDAO.findAll();
        if (listr != null) {
            for (Reply l : listr) {
                replyDTO rDTO = new replyDTO();
                rDTO.setReply(l);
                rDTO.setReply_vote(reply_voteDAO.findAll());
                list.add(rDTO);
            }
        }
        return list;
    }
    @GetMapping("/tk/{x}/{y}")
    public ResponseEntity< List<CountMonth>> tk(@PathVariable("x") java.sql.Date from, @PathVariable("y") java.sql.Date to) {
        Calendar c = Calendar.getInstance();
        c.setTime(to);
        c.add(Calendar.DATE,1);
        List<String> list= replyDAO.findtk1(from,c.getTime());
        List<CountMonth> countMonths =new ArrayList<>();
        for (String n : list){
            CountMonth CountMonth =new CountMonth();
            String[] arr = n.split(",");
            String arr1= arr[0];
            Integer integer =arr[1]!=null?Integer.valueOf(arr[1]):0;
            CountMonth.setDay(arr1!=null?arr1:null);
            CountMonth.setCount(integer);
            countMonths.add(CountMonth);
        }
        return ResponseEntity.ok(countMonths);
    }
}
