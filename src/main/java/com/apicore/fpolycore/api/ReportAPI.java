package com.apicore.fpolycore.api;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;

import com.apicore.fpolycore.Class.*;
import com.apicore.fpolycore.DAO.*;
import com.apicore.fpolycore.DTO.CountMonth;
import com.apicore.fpolycore.DTO.ReportsquestionDTO;
import com.apicore.fpolycore.DTO.ReportsreplyDTO;
import com.apicore.fpolycore.DTO.reportforuser;
import com.fasterxml.jackson.annotation.JacksonInject.Value;

import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin("*")
@RestController
@RequestMapping("report")
public class ReportAPI {
    @Autowired
    reportDAO reportDAO;
    @Autowired
    UsersDAO usersDAO;
    @Autowired
    ReportReplyDAO reportReplyDAO;
    @Autowired
    ReplyDAO replydao;
    @Autowired
    ReportQuestionDAO reportQuestionDAO;
    @Autowired
    QuestionDAO questionDAO;

    @GetMapping("/get")
    public ResponseEntity<List<Reports>> getreport() {
        return ResponseEntity.ok(reportDAO.findAll());
    }

    @GetMapping("/get/{x}")
    public ResponseEntity<Optional<Reports>> get(@PathVariable("x") Integer id) {
        return ResponseEntity.ok(reportDAO.findById(id));
    }

    @GetMapping("/getBycontent/{x}")
    public ResponseEntity<List<Reports>> getByContent(@PathVariable("x") String str) {
        return ResponseEntity.ok(reportDAO.findByContent(str));
    }

    @GetMapping("/getBycontentDate/{x}/{y}/{z}")
    public ResponseEntity<List<String>> getByContent(@PathVariable("x") String str, @PathVariable("y") java.sql.Date from, @PathVariable("z") java.sql.Date to) {
        return ResponseEntity.ok(reportDAO.findByContentBetween(str, from, to));
    }

    @PostMapping("/question")
    public ResponseEntity<String> postquestion(@Validated @RequestBody ReportsquestionDTO reportsquestionDTO, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.badRequest().build();
        } else {
            if (questionDAO.existsById(reportsquestionDTO.getQuestion()) && usersDAO.existsById(reportsquestionDTO.getIdusers())) {
                Reports reports = new Reports();
                reports.setContent(reportsquestionDTO.getContent());
                reports.setStatus(false);
                reports.setUsers(usersDAO.getById(reportsquestionDTO.getIdusers()));
//                LocalDateTime localNow = LocalDateTime.now();
//             ZonedDateTime zonedUTC = localNow.atZone(ZoneId.of("UTC"));
//    			Timestamp timestamp = Timestamp.valueOf(zonedUTC.toLocalDateTime());
            	Date now = new Date();
        		Timestamp timestamp = new Timestamp(now.getTime());
    	    	System.out.println(timestamp);
                reports.setCreateAt(timestamp);
                Reports datareturn = reportDAO.save(reports);
                ReportQuestion question = new ReportQuestion();
                question.setReport(datareturn);
                question.setQuestion(questionDAO.getById(reportsquestionDTO.getQuestion()));
                reportQuestionDAO.save(question);
                return ResponseEntity.ok("thanh cong");
            } else {
                return ResponseEntity.notFound().build();
            }
        }

    }

    @PostMapping("/reply")
    public ResponseEntity<String> postreply(@Validated @RequestBody ReportsreplyDTO reportsreplyDTO, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.badRequest().build();
        } else {
            if (replydao.existsById(reportsreplyDTO.getReply()) && usersDAO.existsById(reportsreplyDTO.getIdusers())) {
                Reports reports = new Reports();
                reports.setContent(reportsreplyDTO.getContent());
                reports.setStatus(false);
                reports.setUsers(usersDAO.getById(reportsreplyDTO.getIdusers()));
                Date now = new Date();
    			Timestamp timestamp = new Timestamp(now.getTime());
                reports.setCreateAt(timestamp);
                Reports datareturn = reportDAO.save(reports);
                ReportReply question = new ReportReply();
                question.setReport(datareturn);
                question.setReply(replydao.getById(reportsreplyDTO.getReply()));
                reportReplyDAO.save(question);
                return ResponseEntity.ok("thanh cong");
            } else {
                return ResponseEntity.badRequest().build();
            }
        }
    }

    @PostMapping("/postUser")
    public ResponseEntity<Reports> postUser(@Validated @RequestBody reportforuser reportforusers, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.badRequest().build();
        } else {
            Users users = usersDAO.getById(reportforusers.getIdUser());
            if (users != null) {
                Reports report = new Reports();
                report.setContent(reportforusers.getContent());
                report.setStatus(false);
                Date now = new Date();
    			Timestamp timestamp = new Timestamp(now.getTime());
                report.setCreateAt(timestamp);
                report.setUsers(users);
                Reports reports = reportDAO.save(report);
                if (reportforusers.getType() == true) {
                    // reply
                    Reply reply1 = replydao.getById(reportforusers.getIdReport());
                    if (reply1 != null) {
                        ReportReply reportReply = new ReportReply();
                        reportReply.setReply(reply1);
                        reportReply.setReport(reports);
                        reportReplyDAO.save(reportReply);
                    } else {
                        return ResponseEntity.badRequest().build();
                    }
                } else {
                    //que
                    Question question = questionDAO.getById(reportforusers.getIdReport());
                    if (question != null) {
                        ReportQuestion reportQuestion = new ReportQuestion();
                        reportQuestion.setQuestion(question);
                        reportQuestion.setReport(reports);
                        reportQuestionDAO.save(reportQuestion);
                    } else {
                        return ResponseEntity.badRequest().build();
                    }
                }

            } else {
                return ResponseEntity.badRequest().build();
            }

//			if (reportDAO.existsById(report.getId())) {
//				return ResponseEntity.badRequest().build();
//			}
//			reportDAO.save(report);
            return ResponseEntity.ok(null);
        }

    }

    @PutMapping("/put/{x}")
    public ResponseEntity<Reports> put(@PathVariable("x") Integer id, @Validated @RequestBody Reports report, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.badRequest().build();
        } else {
            if (reportDAO.existsById(report.getId()) && id == report.getId()) {
                reportDAO.save(report);
                return ResponseEntity.ok(report);
            }
            return ResponseEntity.badRequest().build();
        }
    }

    @DeleteMapping("/delete/{x}")
    public ResponseEntity<Void> delete(@PathVariable("x") Integer id) {
        if (!reportDAO.existsById(id)) {
            return ResponseEntity.ok().build();
        }
        reportDAO.deleteById(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/tk/{x}/{y}")
    public ResponseEntity< List<CountMonth>> tk(@PathVariable("x") java.sql.Date from, @PathVariable("y") java.sql.Date to) {
        Calendar c = Calendar.getInstance();
        c.setTime(to);
        c.add(Calendar.DATE,1);
        List<String> list= reportDAO.findtk(from,c.getTime());
        List<CountMonth> countMonths =new ArrayList<>();
        for (String n : list){
            CountMonth CountMonth =new CountMonth();
            String[] arr = n.split(",");
            String arr1= arr[0];
            Integer integer =arr[1]!=null?Integer.valueOf(arr[1]):0;
            CountMonth.setDay(arr1!=null?arr1:null);
            CountMonth.setCount(integer);
            countMonths.add(CountMonth);
        }
        return ResponseEntity.ok(countMonths);
    }
}
