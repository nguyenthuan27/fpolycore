package com.apicore.fpolycore.api;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.apicore.fpolycore.Class.PointHistory;
import com.apicore.fpolycore.Class.Posts;
import com.apicore.fpolycore.Class.Question;
import com.apicore.fpolycore.Class.Reply;
import com.apicore.fpolycore.Class.ReportQuestion;
import com.apicore.fpolycore.Class.ReportReply;
import com.apicore.fpolycore.Class.Reports;
import com.apicore.fpolycore.Class.Users;
import com.apicore.fpolycore.Class.notification;
import com.apicore.fpolycore.Class.user_report;
import com.apicore.fpolycore.DAO.AdminReportDAO;
import com.apicore.fpolycore.DAO.ModReportDAO;
import com.apicore.fpolycore.DAO.MyNoticeDAO;
import com.apicore.fpolycore.DAO.PointHistoryDAO;
import com.apicore.fpolycore.DAO.PostDAO;
import com.apicore.fpolycore.DAO.QuestionDAO;
import com.apicore.fpolycore.DAO.ReplyDAO;
import com.apicore.fpolycore.DAO.ReportQuestionDAO;
import com.apicore.fpolycore.DAO.ReportReplyDAO;
import com.apicore.fpolycore.DAO.User_reportDAO;
import com.apicore.fpolycore.DAO.UsersDAO;
import com.apicore.fpolycore.DAO.reportDAO;
import com.apicore.fpolycore.DTO.AdminConfirmReportDTO;
import com.apicore.fpolycore.DTO.AdminReportDTO;
import com.apicore.fpolycore.DTO.ConfirmReportDTO;
import com.apicore.fpolycore.DTO.ListUser_Report;
import com.apicore.fpolycore.DTO.ModReportDTO;

@CrossOrigin("*")
@RestController
@RequestMapping("AdminReport")
public class AdminReportApi {
    @Autowired
    reportDAO reportDAO;
    @Autowired
    AdminReportDAO adminReportDAO;
    @Autowired
    User_reportDAO userReportDAO;
    @Autowired
    PostDAO postDAO;
    @Autowired
    QuestionDAO questionDAO;
    @Autowired
    ReplyDAO replyDAO;
    @Autowired
    UsersDAO usersDAO;
    @Autowired
    ReportQuestionDAO reportQuestionDAO;
    @Autowired
    ReportReplyDAO reportReplyDAO;
	@Autowired
	PointHistoryDAO pointHistoryDAO;
	@Autowired
	MyNoticeDAO notificationDAO;

    @GetMapping("/get")
    public ResponseEntity<List<AdminReportDTO>> getreport() {
        return ResponseEntity.ok(getList());
    }

    @GetMapping("/get/{x}")
    public ResponseEntity<AdminReportDTO> get(@PathVariable("x") Integer id) {
        return ResponseEntity.ok(getOne(id));
    }

    @PostMapping("/post")
    public ResponseEntity<Reports> post(@Validated @RequestBody Reports report, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.badRequest().build();
        } else {
            if (reportDAO.existsById(report.getId())) {
                return ResponseEntity.badRequest().build();
            }
            reportDAO.save(report);
            return ResponseEntity.ok(report);
        }

    }

    @PostMapping("/revert")
    public ResponseEntity<AdminConfirmReportDTO> revert(@Validated @RequestBody AdminConfirmReportDTO confirmReport,
                                                        BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.badRequest().build();
        } else {
            Reports report = reportDAO.getById(confirmReport.getReportId());
            report.setStatus(true);
            reportDAO.save(report);

            Date now = new Date();
			Timestamp timestamp = new Timestamp(now.getTime());
            if (report != null) {
                if (confirmReport.getStatus() == false) {
                    if (confirmReport.getCheck() == true) {
                        ReportQuestion reportQuestion = reportQuestionDAO.findOne(report.getId());
                        Question question = questionDAO.getById(reportQuestion.getQuestion().getId());
                        Posts post = postDAO.findP(question.getId());
                        if (post != null) {
                            post.setDisplay_status(true);
                            postDAO.save(post);
                            Users user = usersDAO.getById(report.getUsers().getId());
                            user.setPoint(user.getPoint() - 5);
                            usersDAO.save(user);
                            
                            int point = 5;
                            PointHistory ph = new PointHistory();
							ph.setUsers(user);
							ph.setPoint(user.getPoint());
							ph.setCreate_date(timestamp);
							ph.setType("-" + point);
							ph.setNote("report post:" + post.getId());
							pointHistoryDAO.save(ph);

							notification noti = new notification();
							noti.setContent("Bạn đã bị trừ " + point + " khi báo cáo sai bài viết");
							noti.setCreateAt(timestamp);
							noti.setTitle("Trừ điểm report: " + report.getId());
							noti.setType(true);
							noti.setUser_id(user.getId());
							notificationDAO.save(noti);
                        }
                    } else {
                        ReportReply reportReply = reportReplyDAO.findReport(report.getId());
                        Reply reply = replyDAO.getById(reportReply.getReply().getId());
                        if (reply != null) {
                            reply.setStatus(true);
                            replyDAO.save(reply);
                            
                            String str = "reply:" + reply.getId().toString();
							String p = pointHistoryDAO.find(str, reply.getUsers().getId());
							if (p != null) {
								int diem = Integer.parseInt(p.substring(1));
								int point = 5;
								Users user = usersDAO.getById(report.getUsers().getId());
								user.setPoint(user.getPoint() - point);
								usersDAO.save(user);

								PointHistory ph = new PointHistory();
								ph.setUsers(user);
								ph.setPoint(user.getPoint());
								ph.setCreate_date(timestamp);
								ph.setType("-" + point);
								ph.setNote("report reply:" + reply.getId());
								pointHistoryDAO.save(ph);

								notification noti = new notification();
								noti.setContent("Bạn đã bị trừ " + point + " khi báo cáo sai câu trả lời");
								noti.setCreateAt(timestamp);
								noti.setTitle("Trừ điểm report: " + report.getId());
								noti.setType(true);
								noti.setUser_id(user.getId());
								notificationDAO.save(noti);
                        }

                        }
                    }

                } else {
                    Users user = usersDAO.getById(report.getUsers().getId());
                    user.setPoint(user.getPoint());
                    usersDAO.save(user);
                }

            }
        }

        return ResponseEntity.ok(confirmReport);
    }

    @PostMapping("/confirmtrue")
	public ResponseEntity<ConfirmReportDTO> post(@Validated @RequestBody ConfirmReportDTO confirmReport,
			BindingResult result) {
		if (result.hasErrors()) {
			return ResponseEntity.badRequest().build();
		} else {
			user_report userReport = new user_report();
			userReport.setUsers(usersDAO.getById(confirmReport.getConfirmUser()));
			userReport.setStatus(confirmReport.getStatus());

			Date now = new Date();
			Timestamp timestamp = new Timestamp(now.getTime());
			userReport.setCreateAt(timestamp);
			userReport.setReport(reportDAO.getById(confirmReport.getReportId()));
			userReportDAO.save(userReport);

			Reports report = reportDAO.getById(confirmReport.getReportId());
			report.setStatus(false);
			reportDAO.save(report);

			if (report != null) {
				if (confirmReport.getStatus() == true) {
					if (confirmReport.getCheck() == true) {
						ReportQuestion reportQuestion = reportQuestionDAO.findOne(report.getId());
						Question question = questionDAO.getById(reportQuestion.getQuestion().getId());
						Posts post = postDAO.findP(question.getId());
						if (post != null) {
							System.out.println(post.getDisplay_status());
							post.setDisplay_status(false);
							postDAO.save(post);
							
							Users user = usersDAO.getById(report.getUsers().getId());
							int point = 5;
							user.setPoint(user.getPoint() + point);
							usersDAO.save(user);

							PointHistory ph = new PointHistory();
							ph.setUsers(user);
							ph.setPoint(user.getPoint());
							ph.setCreate_date(timestamp);
							ph.setType("+" + point);
							ph.setNote("report post:" + post.getId());
							pointHistoryDAO.save(ph);

							notification noti = new notification();
							noti.setContent("Bạn đã được cộng " + point + " khi báo cáo bài viết");
							noti.setCreateAt(timestamp);
							noti.setTitle("Cộng điểm report: " + report.getId());
							noti.setType(true);
							noti.setUser_id(user.getId());
							notificationDAO.save(noti);
						}

					} else {
						ReportReply reportReply = reportReplyDAO.findReport(report.getId());
						Reply reply = replyDAO.getById(reportReply.getReply().getId());
						if (reply != null) {
							reply.setStatus(false);
							replyDAO.save(reply);
							
							String str = "reply:" + reply.getId().toString();
							String p = pointHistoryDAO.find(str, reply.getUsers().getId());
							if (p != null) {
								int diem = Integer.parseInt(p.substring(1));
								int point = 5;
								Users user = usersDAO.getById(report.getUsers().getId());
								user.setPoint(user.getPoint() + point);
								usersDAO.save(user);

								PointHistory ph = new PointHistory();
								ph.setUsers(user);
								ph.setPoint(user.getPoint());
								ph.setCreate_date(timestamp);
								ph.setType("+" + point);
								ph.setNote("report reply:" + reply.getId());
								pointHistoryDAO.save(ph);

								notification noti = new notification();
								noti.setContent("Bạn đã được cộng " + point + " khi báo cáo câu trả lời");
								noti.setCreateAt(timestamp);
								noti.setTitle("Cộng điểm report: " + report.getId());
								noti.setType(true);
								noti.setUser_id(user.getId());
								notificationDAO.save(noti);
							}
						}
					}

				} else {
					Users user = usersDAO.getById(report.getUsers().getId());
					user.setPoint(user.getPoint() - 10);
					usersDAO.save(user);

					PointHistory ph = new PointHistory();
					ph.setUsers(user);
					ph.setPoint(user.getPoint());
					ph.setCreate_date(timestamp);
					ph.setType("-10");
					ph.setNote("report:" + report.getId());
					pointHistoryDAO.save(ph);

					notification noti = new notification();
					noti.setContent("Bạn đã bị trừ " + 10 + " khi báo cáo sai");
					noti.setCreateAt(timestamp);
					noti.setTitle("Trừ điểm report: " + report.getId());
					noti.setType(true);
					noti.setUser_id(user.getId());
					notificationDAO.save(noti);
				}
			}
		}
		return ResponseEntity.ok(confirmReport);
	}
    
    @PostMapping("/confirm/{x}")
    public ResponseEntity<Integer> ok(@PathVariable("x") Integer id) {
        if (id != null) {
            Reports report = reportDAO.getById(id);
            if (report != null) {
                report.setStatus(true);
                reportDAO.save(report);
            }
        }
        return ResponseEntity.ok().build();
    }

    @PutMapping("/put/{x}")
    public ResponseEntity<Reports> put(@PathVariable("x") Integer id, @Validated @RequestBody Reports report,
                                       BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.badRequest().build();
        } else {
            if (reportDAO.existsById(report.getId()) && id == report.getId()) {
                reportDAO.save(report);
                return ResponseEntity.ok(report);
            }
            return ResponseEntity.badRequest().build();
        }
    }

    @PutMapping("/confirmReportQuestion/{x}")
    public ResponseEntity<Question> put(@PathVariable("x") Integer id,
                                        @Validated @RequestBody ReportQuestion reportQuestion, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.badRequest().build();
        } else {
            Question question = questionDAO.findP(reportQuestion.getId());
            Posts p = postDAO.findQ(question.getId());
            p.setDisplay_status(false);
            postDAO.save(p);
        }
        return ResponseEntity.badRequest().build();
    }

    @GetMapping("/confirmReportReply/{x}")
    public ResponseEntity<Question> put(@PathVariable("x") Integer id, @Validated @RequestBody ReportReply reportReply,
                                        BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.badRequest().build();
        } else {
            Reply reply = replyDAO.findQ(reportReply.getId());
            reply.setStatus(false);
            replyDAO.save(reply);
        }
        return ResponseEntity.badRequest().build();
    }

    @DeleteMapping("/delete/{x}")
    public ResponseEntity<Void> delete(@PathVariable("x") Integer id) {
        if (!reportDAO.existsById(id)) {
            return ResponseEntity.ok().build();
        }
        reportDAO.deleteById(id);
        return ResponseEntity.ok().build();
    }

    public List<AdminReportDTO> getList() {
        List<Reports> listReport = reportDAO.findAll();
        List<AdminReportDTO> list = new ArrayList<>();
        for (Reports dl : listReport) {
            if (dl.getStatus() == false) {
                List<user_report> urp = userReportDAO.findU(dl.getId());
                ReportQuestion reportq = reportQuestionDAO.findOne(dl.getId());
                ReportReply reportr = reportReplyDAO.findReport(dl.getId());
                if (reportq == null && reportr != null) {
                    if (urp.size() == 0) {
                        list.add(new AdminReportDTO(dl, false, null, reportr.getReply()));
                    } else {
                        list.add(new AdminReportDTO(dl, true, null, reportr.getReply()));
                    }
                } else if (reportq != null && reportr == null) {
                    if (urp.size() == 0) {
                        list.add(new AdminReportDTO(dl, false, reportq.getQuestion(), null));
                    } else {
                        list.add(new AdminReportDTO(dl, true, reportq.getQuestion(), null));
                    }
                }

            }
        }
        return list;
    }

    public AdminReportDTO getOne(Integer id) {
        Reports report = reportDAO.find(id);
        AdminReportDTO dto = new AdminReportDTO();
        if (report.getStatus() == false) {
            List<user_report> urp = userReportDAO.findU(report.getId());
            ReportQuestion reportq = reportQuestionDAO.findOne(report.getId());
            ReportReply reportr = reportReplyDAO.findReport(report.getId());
            if (reportq == null && reportr != null) {
                if (urp.size() == 0) {
                    dto = new AdminReportDTO(report, false, null, reportr.getReply());
                } else {
                    dto = new AdminReportDTO(report, true, null, reportr.getReply());
                }
            } else if (reportq != null && reportr == null) {
                if (urp.size() == 0) {
                    dto = new AdminReportDTO(report, false, reportq.getQuestion(), null);
                } else {
                    dto = new AdminReportDTO(report, true, reportq.getQuestion(), null);
                }
            }
        }
        return dto;
    }

    @PostMapping("/confirmReport")
    public ResponseEntity<AdminConfirmReportDTO> approval(@Validated @RequestBody AdminConfirmReportDTO confirmReport,
                                                          BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.badRequest().build();
        } else {
            user_report userReport = new user_report();
            userReport.setUsers(usersDAO.getById(confirmReport.getConfirmUser()));
            userReport.setStatus(confirmReport.getStatus());

            Date now = new Date();
            Timestamp timestamp = new Timestamp(now.getTime());
            userReport.setCreateAt(timestamp);
            userReport.setReport(reportDAO.getById(confirmReport.getReportId()));
            userReportDAO.save(userReport);

            Reports report = reportDAO.getById(confirmReport.getReportId());
            report.setStatus(true);
            reportDAO.save(report);

            if (report != null) {
                if (confirmReport.getStatus() == true) {
                    if (confirmReport.getCheck() == true) {
                        ReportQuestion reportQuestion = reportQuestionDAO.findOne(report.getId());
                        Question question = questionDAO.getById(reportQuestion.getQuestion().getId());
                        Posts post = postDAO.findP(question.getId());
                        if (post != null) {
                            System.out.println(post.getDisplay_status());
                            post.setDisplay_status(false);
                            postDAO.save(post);
                            Users user = usersDAO.getById(report.getUsers().getId());
                            user.setPoint(user.getPoint() + 5 + (question.getPoint() * 10 / 100));
                            usersDAO.save(user);
                        }

                    } else {
                        ReportReply reportReply = reportReplyDAO.findReport(report.getId());
                        Reply reply = replyDAO.getById(reportReply.getReply().getId());
                        if (reply != null) {
                            System.out.println(reply.getStatus());
                            reply.setStatus(false);
                            replyDAO.save(reply);
                        }

//						Users user = usersDAO.getById(report.getId());
//						user.setPoint(user.getPoint()+5); //thêm 10% diem của reply
//						usersDAO.save(user);
                    }

                } else {
                    Users user = usersDAO.getById(report.getUsers().getId());
                    user.setPoint(user.getPoint() - 10);
                    usersDAO.save(user);
                }
            }
        }
        return ResponseEntity.ok(confirmReport);
    }

}
