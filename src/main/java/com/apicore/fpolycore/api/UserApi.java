package com.apicore.fpolycore.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.apicore.fpolycore.Class.*;
import com.apicore.fpolycore.DAO.*;
import com.apicore.fpolycore.DTO.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin("*")
@RestController
@RequestMapping("user")
public class UserApi {
    @Autowired
    UsersDAO usersDAO;
    @Autowired
    Users_PostsDAO users_postsDAO;
    @Autowired
    Post_VoteDAO post_voteDAO;
    @Autowired
    ReplyDAO replyDAO;
    @Autowired
    PostDAO postDAO;
    @Autowired
    QuestionDAO QuestionDAO;
    @Autowired
    SubjectDAO subjectDAO;
    @Autowired
    Reply_VoteDAO reply_voteDAO;
    @Autowired
    BranchesDAO branchesDAO;

    @GetMapping("/get")
    public ResponseEntity<List<Users>> getfull() {
        return ResponseEntity.ok(usersDAO.findAll());
    }

    @GetMapping("/getListOfPost/{x}")
    public ResponseEntity<List<postOfUserDTO>> getPost(@PathVariable("x") Integer id) {
        return ResponseEntity.ok(getListPost(id));
    }

    @GetMapping("/getListOfReply/{x}")
    public ResponseEntity<List<replyOfUserDTO>> getReply(@PathVariable("x") Integer id) {
        return ResponseEntity.ok(getListReply(id));
    }
    
    @GetMapping("/get/{x}")
    public ResponseEntity<Optional<Users>> get(@PathVariable("x") Integer id) {
        return ResponseEntity.ok(usersDAO.findById(id));
    }

    @GetMapping("/getProfile/{x}")
    public ResponseEntity<ProfileUser> getProfiles(@PathVariable("x") Integer id) {
        return ResponseEntity.ok(getProfile(id));
    }

    @PostMapping("/post")
    public ResponseEntity<Users> post(@RequestBody Users users, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.badRequest().build();
        } else {
            if (usersDAO.existsById(users.getId())) {
                return ResponseEntity.badRequest().build();
            }
            usersDAO.save(users);
            return ResponseEntity.ok(users);
        }

    }
    
    @PutMapping("/putProfile/{x}")
    public ResponseEntity<String> putUserProfile(@PathVariable("x") Integer id, @Validated @RequestBody userProfileDTO users, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.badRequest().build();
        } else {
            if (usersDAO.existsById(id)) {
            	Users u = usersDAO.getById(id);
            	if(u!=null) {
            		if(users.getFullname()==null) {
            			u.setFullname(u.getFullname());
            		}else {
            			u.setFullname(users.getFullname());
            		}
            		if(users.getImage()==null) {
            			u.setImage(u.getImage());
            		}else {
            			u.setImage(users.getImage());
            		}
            		if(users.getBranchId()==null) {
            			u.setBranch(u.getBranch());
            		}else {
            			Branches branch = branchesDAO.getById(users.getBranchId());
            			if(branch!=null) {
            				u.setBranch(branch);
            			}
            			
            		}
            	}
                usersDAO.save(u);
                return ResponseEntity.ok("Cập nhật thành công!");
            }
            return ResponseEntity.badRequest().build();
        }
    }
            
    @PutMapping("/put/{x}")
    public ResponseEntity<Users> put(@PathVariable("x") Integer id, @Validated @RequestBody Users users, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.badRequest().build();
        } else {
        	if (usersDAO.existsById(users.getId()) || id == users.getId()) {
                usersDAO.save(users);
                return ResponseEntity.ok(users);
            }
            return ResponseEntity.badRequest().build();
        }

    }

//	@PutMapping("/setImage/{x}")
//	public ResponseEntity<Users> setImage(@PathVariable("x") Integer id,@Validated @RequestBody  String img, BindingResult result){
//		if(result.hasErrors()) {
//			return ResponseEntity.badRequest().build();
//		}else {
//			if(usersDAO.getById(id) != null) {
//				Users u = usersDAO.getById(id);
//				u.setFullname(usersDAO.getById(id).getFullname());
//				u.setCreate_at(usersDAO.getById(id).getCreate_at());
//				u.setEmail(usersDAO.getById(id).getEmail());
//				u.setPoint(usersDAO.getById(id).getPoint());
//				u.setRole(usersDAO.getById(id).getRole());
//				u.setImage(img);
//				usersDAO.save(u);
//					return ResponseEntity.ok(u);
//			}
//			return ResponseEntity.badRequest().build();
//		}
//	
//	}

    @DeleteMapping("/delete/{x}")
    public ResponseEntity<Void> delete(@PathVariable("x") Integer id) {
        if (!usersDAO.existsById(id)) {
            return ResponseEntity.ok().build();
        }

        usersDAO.deleteById(id);
        return ResponseEntity.ok().build();
    }

    public List<UserDTO> getList() {
        List<UserDTO> list = new ArrayList<UserDTO>();
        List<Users> listu = usersDAO.findAll();
        for (Users ul : listu) {
            UserDTO udto = new UserDTO();
            udto.setUsers(ul);
            list.add(udto);

        }
        return list;
    }

    public List<postOfUserDTO> getListPost(Integer id) {
        List<postOfUserDTO> listPostDTO = new ArrayList<>();
        if (listPostDTO != null) {
            List<Posts> listp = postDAO.findByUserId(id);
            if (listp != null) {
                for (Posts p : listp) {
                    Question q = QuestionDAO.findOne(p.getId());
                    if (q != null) {
                        postOfUserDTO pDTO = new postOfUserDTO(p.getId(), q.getPoint(), p.getCreateAt(),
                                q.getContent());
                        listPostDTO.add(pDTO);
                    }

                }
            }

        }

        return listPostDTO;

    }

    public List<replyOfUserDTO> getListReply(Integer id) {
        List<replyOfUserDTO> listReplyDTO = new ArrayList<>();

        if (listReplyDTO != null) {
            List<Reply> listReply = replyDAO.findByUserId(id);
            if (listReply != null) {
                for (Reply dl : listReply) {
                    replyOfUserDTO rDTO = new replyOfUserDTO(dl.getId(), dl.getCreateAt(), dl.getContent());
                    listReplyDTO.add(rDTO);
                }
            }
        }
        return listReplyDTO;
    }
    
    //api profile
    public ProfileUser getProfile(Integer id) {
        ProfileUser listReplyDTO = new ProfileUser();
        List<replyv2DTO> listrComment = new ArrayList<>();
        List<questionv2sDTO> listrDetailedComment = new ArrayList<>();
        List<Reply> listreply = replyDAO.findByUserId(id);
        List<Posts> listpost = postDAO.findByUserId(id);
        if (listreply.size() > 0 || listpost.size() > 0) {
            for (Reply data : listreply) {
                replyv2DTO replyv2DTO = new replyv2DTO(data.getId(), data.getQuestion().getPosts().getId(), data.getContent(), data.getType() == true ? 1 : 0, data.getAnonymus() ? 1 : 0, data.getStatus() ? 1 : 0, data.getImg(), data.getCreateAt(), reply_voteDAO.CountLike(data.getId()), reply_voteDAO.CountDis(data.getId()));
                listrComment.add(replyv2DTO);
            }
            for (Posts data : listpost) {
                PageRequest page = PageRequest.of(0, 1);
                Users_Posts aBoolean = users_postsDAO.findAllbyId1(data.getId(), page);
                if (aBoolean == null ? aBoolean == null : aBoolean.getStatus() == true) {
                    Question listq = QuestionDAO.findQ(data.getId());
                    if (listq != null) {
                        List<TagDTO> listtaDtos = new ArrayList<>();
                        List<Subject> sub12 = subjectDAO.findNew(data.getId());
                        List<Reply> listrep = replyDAO.find(listq.getId(), true);
                        List<Reply> listrepP = replyDAO.find2(listq.getId(), false);
                        if (sub12.size() > 0) {
                            for (Subject sub1 : sub12) {
                                listtaDtos.add(new TagDTO(sub1.getId(), sub1.getName()));
                            }
                            questionv2sDTO questionv2sDTO = new questionv2sDTO(listq.getId(), data.getId(), listq.getTitle(), listq.getContent(), data.getType() == true ? 1 : 0, data.getAnonymus() ? 1 : 0, data.getDisplay_status() ? 1 : 0, listq.getImg(), listq.getCreateAt(), listrep.size(), listrepP.size(),listq.getPoint(),listtaDtos);
                            listrDetailedComment.add(questionv2sDTO);
                        }
                    }
                }
            }
            if(listrDetailedComment.size() >0){
                listReplyDTO.setListrComment(listrComment);
                listReplyDTO.setListrDetailedComment(listrDetailedComment);
            }
        }
        return listReplyDTO;
    }
}
