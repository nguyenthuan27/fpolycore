package com.apicore.fpolycore.api;

import com.apicore.fpolycore.ClassRetrun.statisticalpost;
import com.apicore.fpolycore.DAO.*;
import com.apicore.fpolycore.DTO.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.WeekFields;
import java.util.*;

@CrossOrigin("*")
@RestController
@RequestMapping("statistical")
public class Statistical {
    @Autowired
    ReplyDAO replyDAO;
    @Autowired
    UsersDAO usersDAO;

    @GetMapping("/get")
    public ResponseEntity<List<StatisticalCount>> getreport() {
        return ResponseEntity.ok(getList());
    }


    @GetMapping("/gettime/{x}")
    public ResponseEntity<List<StatisticalCount>> getreport1(@PathVariable("x") Integer id) {
        return ResponseEntity.ok(getListBuyTime(id));
    }
//
//    @PostMapping("/post")
//    public ResponseEntity<List<StatisticalCount>> post(@Validated @RequestBody statisticalpost statisticalpost , BindingResult result) {
//        if (result.hasErrors()) {
//            return ResponseEntity.badRequest().build();
//        } else {
//            return ResponseEntity.ok(getListBuyTime(statisticalpost.getStatus()));
//        }
//
//    }

    public List<StatisticalCount> getList() {
        List<StatisticalCount> comment = new ArrayList<>();
        PageRequest page = PageRequest.of(0, 10);
        List<StatisticalCount> a = replyDAO.findS(page);
        return a;
    }

    public List<StatisticalCount> getListBuyTime(int status) {
        List<StatisticalCount> a = null;
        long millis=System.currentTimeMillis();
        Date date = new Date(millis);
        Calendar c1 = Calendar.getInstance();
        if(status ==1 ){//tìm kiếm 1 theo ngày
            System.out.println(1);
            c1.clear();
            c1.setTime(date);
            c1.add(Calendar.DATE,1);
            a= replyDAO.findS1D(date,c1.getTime());
            System.out.println(date);
            System.out.println(c1.getTime());
        }
        else if(status ==2){// tìm kiếm trong 1 tuần
            System.out.println(2);
            c1.clear();
            c1.setTimeInMillis(millis);
            while (c1.get(Calendar.DAY_OF_WEEK) != c1.MONDAY) {
                c1.add(Calendar.DATE, -1); // Substract 1 day until first day of week.
            }
            a=replyDAO.findS1D(c1.getTime(),date);
        }
        else if(status == 3){//tìm kiếm trong 1 tháng
            c1.clear();
            c1.set(Calendar.DATE,c1.getActualMinimum(Calendar.DAY_OF_MONTH));
            a=replyDAO.findS1D(c1.getTime(),date);
            System.out.println(c1.getTime());
            System.out.println(date);
        }

        return a;
    }
}
