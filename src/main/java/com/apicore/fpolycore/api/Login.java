package com.apicore.fpolycore.api;

import com.apicore.fpolycore.Class.Branches;
import com.apicore.fpolycore.Class.Users;
import com.apicore.fpolycore.DAO.BranchesDAO;
import com.apicore.fpolycore.DAO.UsersDAO;
import com.apicore.fpolycore.DTO.LoginDTO;
import com.apicore.fpolycore.DTO.LoginReturnDTO;
import com.apicore.fpolycore.Sercurity.JWTUtility;
import com.apicore.fpolycore.Sercurity.UserService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;


import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Timestamp;
import java.util.Date;
import java.util.regex.*;
import java.util.Collection;



@RestController
@CrossOrigin("*")
public class Login {
    @Autowired
    UsersDAO usersDAO;
    @Autowired
    BranchesDAO branchesDAO;
    private final JWTUtility jwtUtility;
    private final AuthenticationManager authenticationManager;
    private final UserService userService;


    public Login(AuthenticationManager authenticationManager, JWTUtility jwtUtility, UserService userService, UsersDAO usersDAO) {
        this.authenticationManager = authenticationManager;
        this.jwtUtility = jwtUtility;
        this.userService = userService;
        this.usersDAO = usersDAO;
    }

    @PostMapping("/Login")
    public ResponseEntity<?> authenticate(@RequestBody LoginDTO loginDTO) throws Exception {
        String mailGV ="^[a-z][a-z0-9_\\.]{5,32}@[f][pte]+[\\.][e][d][u][\\.][v][n]$";
        Boolean login = false;
        if(Pattern.matches(mailGV, loginDTO.getEmail())){
        Users users = usersDAO.findemail(loginDTO.getEmail());
        if(users!=null){
            if(users.getBranch().getId()==loginDTO.getCs()){
                if(users.getStatus()==true){
                    UserDetails userDetails = userService.loadUserByUsername(loginDTO.getEmail());
                    Collection<? extends GrantedAuthority> role = userDetails.getAuthorities();
                    final String token = jwtUtility.generateToken(userDetails);
                    LoginReturnDTO dl = new LoginReturnDTO();
                    dl.setToken(token);
                    dl.setRole(role);
                    dl.setUser(users);
                    dl.setNewuser(false);
                    return ResponseEntity.ok(dl);
                }
                else {
                    return ResponseEntity.badRequest().body("tài khoản hiện tại đã bị khóa");
                }
            }
            else {
                return ResponseEntity.badRequest().body("Cơ sở đào tạo không đúng");
            }
        }
        else{
          if(branchesDAO.existsById(loginDTO.getCs())){
              Date now = new Date();
              Timestamp timestamp = new Timestamp(now.getTime());
              Users users1 =new Users();
              users1.setFullname(loginDTO.getUsername());
              users1.setEmail(loginDTO.getEmail());
              users1.setRole("1");
              users1.setCreate_at(timestamp);
              users1.setPoint(50);
              users1.setImage(loginDTO.getImg());
              Branches branches = branchesDAO.findemail(loginDTO.getCs());
              users1.setBranch(branches);
              users1.setStatus(true);
              Users users2= usersDAO.save(users1);
              UserDetails userDetails = userService.loadUserByUsername(loginDTO.getEmail());
              Collection<? extends GrantedAuthority> role = userDetails.getAuthorities();
              final String token = jwtUtility.generateToken(userDetails);
              LoginReturnDTO dl = new LoginReturnDTO();
              dl.setToken(token);
              dl.setRole(role);
              dl.setUser(users2);
              dl.setNewuser(true);
              return ResponseEntity.ok(dl);

          }
          else{
              return ResponseEntity.badRequest().body("lỗi tạo tài khoản không thành côgn");
          }
        }

        }
        else{
            return ResponseEntity.badRequest().body("định dạng mail không hợp lệ");
        }
    }
}