package com.apicore.fpolycore.api;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.apicore.fpolycore.Class.Posts;
import com.apicore.fpolycore.Class.Question;
import com.apicore.fpolycore.Class.Reply;
import com.apicore.fpolycore.Class.Subject;
import com.apicore.fpolycore.Class.post_vote;
import com.apicore.fpolycore.DAO.PostDAO;
import com.apicore.fpolycore.DAO.Post_VoteDAO;
import com.apicore.fpolycore.DAO.QuestionDAO;
import com.apicore.fpolycore.DAO.ReplyDAO;
import com.apicore.fpolycore.DAO.SubjectDAO;
import com.apicore.fpolycore.DAO.UsersDAO;
import com.apicore.fpolycore.DAO.Users_PostsDAO;
import com.apicore.fpolycore.DTO.PostVoteDTO;
import com.apicore.fpolycore.DTO.TagDTO;
import com.apicore.fpolycore.DTO.postForRDTO;
import com.apicore.fpolycore.DTO.questionVoteDTO;
import com.apicore.fpolycore.DTO.replyPostDTO;

@CrossOrigin("*")
@RestController
@RequestMapping("AnonymusPost")
public class AnonymusPostAPI {
    @Autowired
    QuestionDAO questionDAO;
    @Autowired
    PostDAO postDAO;
    @Autowired
    UsersDAO usersDAO;
    @Autowired
    Post_VoteDAO post_voteDAO;
    @Autowired
    ReplyDAO replyDAO;
    @Autowired
    SubjectDAO subjectDAO;
    @Autowired
    Users_PostsDAO userPostDAO;

    @GetMapping("/getAll")
    public ResponseEntity<List<postForRDTO>> getfull() {
        return ResponseEntity.ok(getList());
    }

    @GetMapping("/getById/{x}")
    public ResponseEntity<postForRDTO> get(@PathVariable("x") Integer id) {
        return ResponseEntity.ok(getPostById(id));
    }

    @PostMapping("/post")
    public ResponseEntity<Posts> post(@RequestBody Posts post, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.badRequest().build();
        } else {
            if (postDAO.existsById(post.getId())) {
                return ResponseEntity.badRequest().build();
            }
            post.setAnonymus(true);
            post.setDisplay_status(false);
            postDAO.save(post);
            return ResponseEntity.ok(post);
        }
    }


    @PutMapping("/put/{x}")
    public ResponseEntity<Question> put(@PathVariable("x") String id, @RequestBody Question question) {
        return ResponseEntity.ok(question);
    }

    @DeleteMapping("/delete/{x}")
    public ResponseEntity<Void> delete(@PathVariable("x") String id) {
        return ResponseEntity.ok().build();
    }

    public List<postForRDTO> getList() {
        List<Posts> listPost = postDAO.findAll();
        List<postForRDTO> list = new ArrayList<postForRDTO>();
        if (listPost != null) {
            for (Posts dl : listPost) {
                List<PostVoteDTO> listpv = new ArrayList<>();
                questionVoteDTO qdto = new questionVoteDTO();
                TagDTO tdto = new TagDTO();
                Question q = questionDAO.findQ(dl.getId());
                Question dateold = questionDAO.findNew(dl.getId());
                Subject sub = subjectDAO.find(dl.getId());
                List<post_vote> pv = post_voteDAO.find(dl.getId());
                List<replyPostDTO> listrepPostdtode = new ArrayList<>();

                if (q != null) {
                    qdto = new questionVoteDTO(q.getId(), q.getContent(), q.getPoint(), q.getImg(), q.getTitle(), q.getCreateAt(), dateold.getCreateAt());
                }
                if (sub != null) {
                    tdto.setId(sub.getId());
                    tdto.setName(sub.getName());
                }
                for (post_vote postv : pv) {
                    listpv.add(new PostVoteDTO(postv.getUsers(), postv.getLikes() == true ? 1 : 0, postv.getDislike() == true ? 1 : 0));
                }
                List<TagDTO> listtaDtos = new ArrayList<>();
                List<Subject> sub12 = subjectDAO.findNew(dl.getId());
                if (sub12.size() > 0) {
                    for (Subject sub1 : sub12) {
                        listtaDtos.add(new TagDTO(sub1.getId(), sub1.getName()));
                    }
                    if (q != null) {
                        List<Reply> listrepP = replyDAO.find2(q.getId(), true);
                        for (Reply reply : listrepP) {
                            replyPostDTO replydto = new replyPostDTO(reply.getId(), reply.getUsers(), reply.getContent(), reply.getType() == true ? 1 : 0, reply.getAnonymus() == true ? 1 : 0, reply.getStatus() == true ? 1 : 0, reply.getImg(), reply.getCreateAt());
                            listrepPostdtode.add(replydto);
                        }
                    }
                    postForRDTO pDTO = new postForRDTO(dl.getId(), dl.getType() == true ? 1 : 0, dl.getAnonymus() == false ? 0 : 1, dl.getDisplay_status() == true ? 1 : 0, qdto, dl.getUsers(), post_voteDAO.CountDis(dl.getId()), post_voteDAO.CountLike(dl.getId()), questionDAO.Count(dl.getId()), listrepPostdtode, listtaDtos);
                    list.add(pDTO);
                }
            }
        }
        return list;

    }

    public postForRDTO getPostById(Integer id) {
        Posts p = postDAO.findOne(id);
        postForRDTO pDTO = new postForRDTO();
        Question q = questionDAO.findOne(p.getId());
        if (q != null && p != null) {
            Question dateold = questionDAO.findNew(p.getId());
            questionVoteDTO qdto = new questionVoteDTO(q.getId(), q.getContent(), q.getPoint(), q.getImg(), q.getTitle(), q.getCreateAt(), dateold.getCreateAt());
            List<TagDTO> listtaDtos = new ArrayList<>();
            List<Subject> sub = subjectDAO.findNew(p.getId());
            List<replyPostDTO> listrepPostdtode = new ArrayList<>();
            if (sub.size() > 0) {
                for (Subject sub1 : sub) {
                    listtaDtos.add(new TagDTO(sub1.getId(), sub1.getName()));
                }
                if (q != null) {
                    List<Reply> listrepP = replyDAO.find2(q.getId(), true);
                    for (Reply reply : listrepP) {
                        replyPostDTO replydto = new replyPostDTO(reply.getId(), reply.getUsers(), reply.getContent(), reply.getType() == true ? 1 : 0, reply.getAnonymus() == true ? 1 : 0, reply.getStatus() == true ? 1 : 0, reply.getImg(), reply.getCreateAt());
                        listrepPostdtode.add(replydto);
                    }
                }
                pDTO = new postForRDTO(p.getId(), p.getType() == true ? 1 : 0, p.getAnonymus() == true ? 1 : 0, p.getDisplay_status() == true ? 1 : 0, qdto, p.getUsers(), post_voteDAO.CountDis(id), post_voteDAO.CountLike(id), questionDAO.Count(id), listrepPostdtode, listtaDtos);
            }

        }
        return pDTO;
    }
}

