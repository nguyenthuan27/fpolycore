package com.apicore.fpolycore.api;


import java.sql.Timestamp;
import java.util.ArrayList;

import java.util.Date;
import java.util.List;


import com.apicore.fpolycore.Class.*;
import com.apicore.fpolycore.DAO.*;
import com.apicore.fpolycore.DTO.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin("*")
@RestController
@RequestMapping("post")
public class PostApi {
    @Autowired
    QuestionDAO questionDAO;
    @Autowired
    PostDAO postDAO;
    @Autowired
    UsersDAO usersDAO;
    @Autowired
    Post_VoteDAO post_voteDAO;
    @Autowired
    ReplyDAO replyDAO;
    @Autowired
    SubjectDAO subjectDAO;
    @Autowired
    Users_PostsDAO users_postsDAO;

    @GetMapping("/getAll")
    public ResponseEntity<List<postForRDTOv2>> getfullHD() {
        return ResponseEntity.ok(getList());
    }
    @GetMapping("/getAllBlog")
    public ResponseEntity<List<postForRDTOv2>> getfullBlog() {
        return ResponseEntity.ok(getList1());
    }

    @GetMapping("/get")
    public ResponseEntity<List<postForRDTOv2>> get() {
        return ResponseEntity.ok(getListAll());
    }
    
    @GetMapping("/count")
    public ResponseEntity<List<String>> count() {
    	long millis=System.currentTimeMillis();  
    	java.util.Date date=new java.util.Date(millis);
        return ResponseEntity.ok(postDAO.Count(date));
    }
    
    @GetMapping("/thongke/{x}")
    public ResponseEntity<List<String>> thongke(@PathVariable("x") Integer year) {
        return ResponseEntity.ok(postDAO.getThongKePost(year));
//    	return ResponseEntity.ok(getthongke(year));
    }
    
    @GetMapping("/countBetween/{x}/{y}")
    public ResponseEntity<Integer> countBetween(@PathVariable("x") java.sql.Date from, @PathVariable("y") java.sql.Date to) {
        return ResponseEntity.ok(postDAO.CountBetween(from, to));
    }
    
    @GetMapping("/countMonth/{x}")
    public ResponseEntity<Integer> countInMonth(@PathVariable("x") String month) {
    	long millis=System.currentTimeMillis();  
    	java.util.Date date=new java.util.Date(millis);
        return ResponseEntity.ok(postDAO.CountInMonth(date, month));
    }
    
    @GetMapping("/countQuater/{x}/{y}")
    public ResponseEntity<List<String>> countQuater(@PathVariable("x") Integer num, @PathVariable("x") Integer year) {
        return ResponseEntity.ok(postDAO.CountQuater(year, num));
    }
    
    @GetMapping("/getAllAn")
    public ResponseEntity<List<postForRDTOv2>> getfullAn() {
        return ResponseEntity.ok(getListAn());
    }

    @GetMapping("/getById/{x}")
    public ResponseEntity<postForRDTOv2> get(@PathVariable("x") Integer id) {
        return ResponseEntity.ok(getPostById(id));
    }

    @PostMapping("/post")
    	public ResponseEntity<Posts> post(@RequestBody Posts post, BindingResult result) {
        	if (result.hasErrors()) {
    			return ResponseEntity.badRequest().build();
    		} else {
    			if (postDAO.existsById(post.getId())) {
    				return ResponseEntity.badRequest().build();
    			}
    			postDAO.save(post);
    			return ResponseEntity.ok(post);
    		}

    }

    @PostMapping("/approve")
    public ResponseEntity<String> postapprove(@Validated @RequestBody approve approves, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.badRequest().build();
        } else {
            Date now = new Date();
            Timestamp timestamp = new Timestamp(now.getTime());
            Users users = usersDAO.getById(approves.getIdUser());
            Posts posts = postDAO.getById(approves.getIdapprove());
            if (users != null && posts != null) {
                Users_Posts users_posts = new Users_Posts();
                users_posts.setPosts_id(posts.getId());
                users_posts.setUsers(users);
                users_posts.setCreateAt(timestamp);
                users_posts.setStatus(approves.getStatus());
                users_postsDAO.save(users_posts);
                if (approves.getStatus() == true) {
                    posts.setDisplay_status(true);
                    postDAO.save(posts);
                }
            } else {
                return ResponseEntity.badRequest().build();
            }
            return ResponseEntity.ok("thành công");
        }

    }

    @PutMapping("/put/{x}")
    public ResponseEntity<Posts> put(@PathVariable("x") Integer id, @RequestBody Posts post, BindingResult result) {
    	if (result.hasErrors()) {
			return ResponseEntity.badRequest().build();
		} else {
			if (id == post.getId()  ||  postDAO.existsById(post.getId())) {
				postDAO.save(post);
				return ResponseEntity.ok(post);
			}else {
				return ResponseEntity.badRequest().build();
			}
			
		}

    }

    @PostMapping("/delete/{x}")
    public ResponseEntity<String> delete(@PathVariable("x") Integer id) {
    	if (!postDAO.existsById(id)) {
    		return ResponseEntity.badRequest().build();
		}else {
			if (postDAO.existsById(id)) {
			Posts posts = postDAO.getById(id);
			posts.setDisplay_status(false);
			postDAO.save(posts);
			return ResponseEntity.ok("Thành công");
		}else {
			return ResponseEntity.badRequest().build();
		}
		}
    }

//    public List<ThongKePost> getthongke(Integer year){
//    	List<ThongKePost> list = postDAO.ThongKe(year);
//    	List<ThongKePost> listTk = new ArrayList<ThongKePost>();
//    	if(list!=null) {
//    		for(String  dl:list) {
//    			System.out.println(list);
//    		char month = list.get(0).charAt(0);
//    		char number = list.get(0).charAt(2);
//    		System.out.println(month +" "+  number);
//    		
//    		ThongKePost listTk1 = new ThongKePost((int) month -48, (int)number-48);
//    		    		listTk.add(listTk1);
//    		}
//    	}
//    	
//    	return listTk;
//    }
    public List<postForRDTOv2>getList() {
        List<Posts> listPost = postDAO.findAS();
        List<postForRDTOv2> list = new ArrayList<postForRDTOv2>();
        if (listPost != null) {
            for (Posts dl : listPost) {
                PageRequest page = PageRequest.of(0, 1);
                Users_Posts aBoolean = users_postsDAO.findAllbyId1(dl.getId(), page);
                if ((aBoolean == null ? aBoolean == null : aBoolean.getStatus() == true)) {
                    if (dl.getDisplay_status() == true ) {
                        Question questionDAOOne = questionDAO.findQ(dl.getId());
                        if (questionDAOOne != null) {
                            List<PostVoteDTO> listpv = new ArrayList<>();
                            questionVoteDTO qdto = new questionVoteDTO();
                            List<post_vote> pv = post_voteDAO.find(dl.getId());
                            qdto = new questionVoteDTO(questionDAOOne.getId(), questionDAOOne.getContent(), questionDAOOne.getPoint(), questionDAOOne.getImg(), questionDAOOne.getTitle(), questionDAOOne.getCreateAt(), dl.getCreateAt());
                            for (post_vote postv : pv) {
                                listpv.add(new PostVoteDTO(postv.getUsers(), postv.getLikes() == true ? 1 : 0, postv.getDislike() == true ? 1 : 0));
                            }
                            List<TagDTO> listtaDtos = new ArrayList<>();
                            List<Subject> sub12 = subjectDAO.findNew(dl.getId());
                            List<Reply> listrep = replyDAO.find(questionDAOOne.getId(), true);
                            List<Reply> listrepP = replyDAO.find2(questionDAOOne.getId(), false);
                            if (sub12.size() > 0) {
                                for (Subject sub1 : sub12) {
                                    listtaDtos.add(new TagDTO(sub1.getId(), sub1.getName()));
                                }
                                postForRDTOv2 pDTO = new postForRDTOv2(dl.getId(), dl.getType() == true ? 1 : 0,dl.getStatus()==null?0:1, dl.getAnonymus() == true ? 1 : 0, dl.getDisplay_status() == true ? 1 : 0, qdto, dl.getUsers(), post_voteDAO.CountDis(dl.getId()), post_voteDAO.CountLike(dl.getId()), listrep.size(), listrepP.size(), listtaDtos);
                                list.add(pDTO);
                            }
                        }
                    }

                }
            }
        }
        return list;
    }

    public List<postForRDTOv2>getList1() {
        List<Posts> listPost = postDAO.findAS1();
        List<postForRDTOv2> list = new ArrayList<postForRDTOv2>();
        if (listPost != null) {
            for (Posts dl : listPost) {
                PageRequest page = PageRequest.of(0, 1);
                Users_Posts aBoolean = users_postsDAO.findAllbyId1(dl.getId(), page);
                if ( aBoolean == null ? aBoolean == null : aBoolean.getStatus() == true ) {
                    if (dl.getDisplay_status() == true ) {
                        Question questionDAOOne = questionDAO.findQ(dl.getId());
                        if (questionDAOOne != null) {
                            List<PostVoteDTO> listpv = new ArrayList<>();
                            List<post_vote> pv = post_voteDAO.find(dl.getId());
                            questionVoteDTO qdto = new questionVoteDTO(questionDAOOne.getId(), questionDAOOne.getContent(), questionDAOOne.getPoint(), questionDAOOne.getImg(), questionDAOOne.getTitle(), questionDAOOne.getCreateAt(), dl.getCreateAt());
                            for (post_vote postv : pv) {
                                listpv.add(new PostVoteDTO(postv.getUsers(), postv.getLikes() == true ? 1 : 0, postv.getDislike() == true ? 1 : 0));
                            }
                            List<TagDTO> listtaDtos = new ArrayList<>();
                            List<Subject> sub12 = subjectDAO.findNew(dl.getId());
                            List<Reply> listrep = replyDAO.find(questionDAOOne.getId(), true);
                            List<Reply> listrepP = replyDAO.find2(questionDAOOne.getId(), false);
                            if (sub12.size() > 0) {
                                for (Subject sub1 : sub12) {
                                    listtaDtos.add(new TagDTO(sub1.getId(), sub1.getName()));
                                }
                                postForRDTOv2 pDTO = new postForRDTOv2(dl.getId(), dl.getType() == true ? 1 : 0,dl.getStatus()==null?0:1, dl.getAnonymus() == true ? 1 : 0, dl.getDisplay_status() == true ? 1 : 0, qdto, dl.getUsers(), post_voteDAO.CountDis(dl.getId()), post_voteDAO.CountLike(dl.getId()), listrep.size(), listrepP.size(), listtaDtos);
                                list.add(pDTO);
                            }
                        }
                    }

                }
            }
        }
        return list;
    }

    public List<postForRDTOv2> getListAll() {
        List<Posts> listPost = postDAO.findAll();
        List<postForRDTOv2> list = new ArrayList<postForRDTOv2>();
        if (listPost != null) {
            for (Posts dl : listPost) {
                    Question questionDAOOne = questionDAO.findQ(dl.getId());
                    if (questionDAOOne != null) {
                        List<PostVoteDTO> listpv = new ArrayList<>();
                        Question dateold = questionDAO.findNew(dl.getId());
                        List<post_vote> pv = post_voteDAO.find(dl.getId());
                        questionVoteDTO qdto = new questionVoteDTO(questionDAOOne.getId(), questionDAOOne.getContent(), questionDAOOne.getPoint(), questionDAOOne.getImg(), questionDAOOne.getTitle(), questionDAOOne.getCreateAt(), dateold.getCreateAt());
                        for (post_vote postv : pv) {
                            listpv.add(new PostVoteDTO(postv.getUsers(), postv.getLikes() == true ? 1 : 0, postv.getDislike() == true ? 1 : 0));
                        }
                        List<TagDTO> listtaDtos = new ArrayList<>();
                        List<Subject> sub12 = subjectDAO.findNew(dl.getId());
                        List<Reply> listrep = replyDAO.find(questionDAOOne.getId(), true);
                        List<Reply> listrepP = replyDAO.find2(questionDAOOne.getId(), false);
                        if (sub12.size() > 0) {
                            for (Subject sub1 : sub12) {
                                listtaDtos.add(new TagDTO(sub1.getId(), sub1.getName()));
                            }

                            postForRDTOv2 pDTO = new postForRDTOv2(dl.getId(), dl.getType() == false ? 0 : 1,dl.getStatus() == null ? 0 : 1 , dl.getAnonymus() == true ? 1 : 0, dl.getDisplay_status() == true ? 1 : 0, qdto, dl.getUsers(), post_voteDAO.CountDis(dl.getId()), post_voteDAO.CountLike(dl.getId()), listrep.size(), listrepP.size(), listtaDtos);
                            list.add(pDTO);
                        
                    }
                }
            }
        }
        return list;
    }


    public List<postForRDTOv2> getListAn() {
        List<Posts> listPost = postDAO.findAllAll();
        List<postForRDTOv2> list = new ArrayList<postForRDTOv2>();
        if (listPost != null) {
            for (Posts dl : listPost) {
                PageRequest page = PageRequest.of(0, 1);
                Users_Posts aBoolean = users_postsDAO.findAllbyId1(dl.getId(), page);
                if (aBoolean == null) {
                    Question questionDAOOne = questionDAO.findQ(dl.getId());
                    if (questionDAOOne != null) {
                        List<PostVoteDTO> listpv = new ArrayList<>();
                        questionVoteDTO qdto = new questionVoteDTO();
                        Question dateold = questionDAO.findNew(dl.getId());
                        List<post_vote> pv = post_voteDAO.find(dl.getId());
                        qdto = new questionVoteDTO(questionDAOOne.getId(), questionDAOOne.getContent(), questionDAOOne.getPoint(), questionDAOOne.getImg(), questionDAOOne.getTitle(), questionDAOOne.getCreateAt(), dateold.getCreateAt());
                        for (post_vote postv : pv) {
                            listpv.add(new PostVoteDTO(postv.getUsers(), postv.getLikes() == true ? 1 : 0, postv.getDislike() == true ? 1 : 0));
                        }
                        List<replyPostDTO> listrepPostdtode = new ArrayList<>();
                        List<TagDTO> listtaDtos = new ArrayList<>();
                        List<Subject> sub12 = subjectDAO.findNew(dl.getId());
                        List<Reply> listrep = replyDAO.find(questionDAOOne.getId(), true);
                        List<Reply> listrepP = replyDAO.find2(questionDAOOne.getId(), false);
                        if (sub12.size() > 0) {
                            for (Subject sub1 : sub12) {
                                listtaDtos.add(new TagDTO(sub1.getId(), sub1.getName()));
                            }
                            postForRDTOv2 pDTO = new postForRDTOv2(dl.getId(), dl.getType() == false ? 0 : 1,dl.getStatus()==null?0:1, dl.getAnonymus() == true ? 1 : 0, dl.getDisplay_status() == true ? 1 : 0, qdto, dl.getUsers(), post_voteDAO.CountDis(dl.getId()), post_voteDAO.CountLike(dl.getId()), listrep.size(), listrepP.size(), listtaDtos);
                            list.add(pDTO);
                        }
                    }

                }
            }
        }
        return list;
    }

    public postForRDTOv2 getPostById(Integer id) {
        Posts p = postDAO.findOne(id);
        postForRDTOv2 pDTO = new postForRDTOv2();
        Question q = questionDAO.findQ(p.getId());
        if (q != null && p != null) {
            List<replyPostDTO> listrepPostdtode = new ArrayList<>();
            List<Reply> listrepP = replyDAO.find2(q.getId(), true);
            Question dateold = questionDAO.findNew(p.getId());
            questionVoteDTO qdto = new questionVoteDTO(q.getId(), q.getContent(), q.getPoint(), q.getImg(), q.getTitle(), q.getCreateAt(), dateold.getCreateAt());
            List<TagDTO> listtaDtos = new ArrayList<>();
            List<Subject> sub = subjectDAO.findNew(p.getId());
            if (sub.size() > 0) {
                for (Subject sub1 : sub) {
                    listtaDtos.add(new TagDTO(sub1.getId(), sub1.getName()));
                }
                pDTO = new postForRDTOv2(p.getId(), p.getType() == true ? 1 : 0,p.getStatus(), p.getAnonymus() == true ? 1 : 0, p.getDisplay_status() == true ? 1 : 0, qdto, p.getUsers(), post_voteDAO.CountDis(id), post_voteDAO.CountLike(id), questionDAO.Count(id), 0, listtaDtos);
            }

        }
        return pDTO;
    }
}
