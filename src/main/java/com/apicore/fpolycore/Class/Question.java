package com.apicore.fpolycore.Class;


import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@SuppressWarnings("serial")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "question")

public class Question implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String content;
    private Integer point;
    private String img;
    private String title;
    @JsonFormat(pattern="yyyy/MM/dd hh:mm:ss")
    private Timestamp createAt;

    @ManyToOne
    @JoinColumn(name = "posts_id")
    Posts posts;

    @JsonIgnore
    @OneToMany(mappedBy = "question")
    List<Reply> reply;

    @JsonIgnore
    @OneToMany(mappedBy = "question")
    List<ReportQuestion> report_question;

}
