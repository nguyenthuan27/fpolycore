package com.apicore.fpolycore.Class;


import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;


import javax.persistence.Entity;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import javax.persistence.Table;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@SuppressWarnings("serial")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "users_posts")

public class Users_Posts implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @NotNull
    private Integer posts_id;
    @NotNull
    private Boolean status;
    @JsonFormat(pattern="YYYY/MM/dd HH:mm:ss")
    private Timestamp createAt;
    @ManyToOne
    @JoinColumn(name = "user_id")
    Users users;
}
