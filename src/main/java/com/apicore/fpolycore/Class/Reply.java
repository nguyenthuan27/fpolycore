package com.apicore.fpolycore.Class;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "reply")
public class Reply {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    Users users;

    @ManyToOne
    @JoinColumn(name = "question_id")
    Question question;

    @NotBlank
    private String content;

    @NotNull
    private Boolean type;

    @NotNull
    private Boolean anonymus;

    @NotNull
    private Boolean status;

    private String img;

    @JsonFormat(pattern="YYYY/MM/dd HH:mm:ss")
    private Timestamp createAt;

    @JsonIgnore
    @OneToMany(mappedBy = "reply")
    List<Reply_Vote> Reply_Vote;

    @JsonIgnore
    @OneToMany(mappedBy = "reply")
    List<ReportReply> report_reply;


}