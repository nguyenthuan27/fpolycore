package com.apicore.fpolycore.Class;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@SuppressWarnings("serial")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "users")

public class Users implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @NotBlank
    private String fullname;
    @NotEmpty
    private String email;
    @NotNull
    private Integer point;
    @NotBlank
    private String role;
    @JsonFormat(pattern="YYYY/MM/dd HH:mm:ss")
    private Timestamp create_at;
    private String image;
    private Boolean status;

    @ManyToOne
    @JoinColumn(name = "branch_id")
    Branches branch;

    @JsonIgnore
    @OneToMany(mappedBy = "users")
    List<Users_Posts> Users_Posts;

    @JsonIgnore
    @OneToMany(mappedBy = "users")
    List<Posts> Posts;

    @JsonIgnore
    @OneToMany(mappedBy = "users")
    List<post_vote> post_vote;

    @JsonIgnore
    @OneToMany(mappedBy = "users")
    List<user_report> user_report;

    @JsonIgnore
    @OneToMany(mappedBy = "users")
    List<Reports> report;

    @JsonIgnore
    @OneToMany(mappedBy = "users")
    List<PointHistory> point_history;
}
