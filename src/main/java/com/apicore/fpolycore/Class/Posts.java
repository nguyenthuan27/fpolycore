package com.apicore.fpolycore.Class;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Entity;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "posts")

public class Posts implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Boolean type;
    private Boolean anonymus;
    private Integer status;
    private Boolean display_status;
    @JsonFormat(pattern="yyyy/MM/dd hh:mm:ss")
    private Timestamp createAt;

    @ManyToOne
    @JoinColumn(name = "user_id")
    Users users;

    @JsonIgnore
    @OneToMany(mappedBy = "posts")
    List<post_vote> Post_Vote;

    @JsonIgnore
    @OneToMany(mappedBy = "posts")
    List<Question> Question;
    
}
